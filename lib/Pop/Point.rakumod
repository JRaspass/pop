class Pop::Point {
    use nqp;

    has num $.x;
    has num $.y;

    my constant HALF-PI = π / 2;

    method zero {
        nqp::create(self)!SET-SELF(0, 0)
    }

    method new ( Real \x, Real \y = x ) {
        nqp::create(self)!SET-SELF(x, y)
    }

    method length { ( $!x² + $!y² ) ** 0.5 }

    method normal {
        my $length = $.length;
        $length ?? self / $length !! $.zero
    }

    method floor {
        nqp::create(self)!SET-SELF( $!x.floor, $!y.floor )
    }

    method round {
        nqp::create(self)!SET-SELF( $!x.round, $!y.round )
    }

    method ceiling {
        nqp::create(self)!SET-SELF( $!x.ceiling, $!y.ceiling )
    }

    multi method add ( Num(Real) \x, Num(Real) \y = x ) {
        nqp::create(self)!SET-SELF(nqp::add_n($!x, x), nqp::add_n($!y, y))
    }
    multi method add ( ::?CLASS:D \b ) {
        nqp::create(self)!SET-SELF(nqp::add_n($!x, b.x), nqp::add_n($!y, b.y))
    }

    multi method mul ( Num(Real) \x, Num(Real) \y = x ) {
        nqp::create(self)!SET-SELF(nqp::mul_n($!x, x), nqp::mul_n($!y, y))
    }
    multi method mul ( ::?CLASS:D \b ) {
        nqp::create(self)!SET-SELF(nqp::mul_n($!x, b.x), nqp::mul_n($!y, b.y))
    }

    multi method sub ( Num(Real) \x, Num(Real) \y = x ) {
        nqp::create(self)!SET-SELF(nqp::sub_n($!x, x), nqp::sub_n($!y, y))
    }
    multi method sub ( ::?CLASS:D \b ) {
        nqp::create(self)!SET-SELF(nqp::sub_n($!x, b.x), nqp::sub_n($!y, b.y))
    }

    multi method div ( Num(Real) \x, Num(Real) \y = x ) {
        nqp::create(self)!SET-SELF(nqp::div_n($!x, x), nqp::div_n($!y, y))
    }
    multi method div ( ::?CLASS:D \b ) {
        nqp::create(self)!SET-SELF(nqp::div_n($!x, b.x), nqp::div_n($!y, b.y))
    }

    method cross ( ::?CLASS:D \b ) {
        nqp::sub_n( nqp::mul_n( $!x, b.y ), nqp::mul_n( $!y, b.x ) )
    }

    method dot ( ::?CLASS:D \b ) {
        nqp::add_n( nqp::mul_n( $!x, b.x ), nqp::mul_n( $!y, b.y ) )
    }

    # Gives the angle in radians relative to the zero point.
    multi method angle {
        atan2( $!x, $!y ) * -1 + HALF-PI
    }

    # Gives the angle in radians realtive to the specified point. The angle
    # will be converted in such a way that it is directly usable with the SDL
    # coordinate system, where the Y-axis increases down.
    multi method angle ( ::?CLASS:D \b ) {
        atan2( .x, .y ) * -1 + HALF-PI with b.sub(self)
    }

    method Bool { self.defined && ( ?$!x || ?$!y ) }

    multi method Real (::?CLASS:D:) { $.length }
    multi method Real (::?CLASS:U:) {
        warn "Use of uninitialized value of type { self.^name } in numeric context";
        0;
    }

    multi method Numeric (::?CLASS:D:) { $.length }
    multi method Numeric (::?CLASS:U:) {
        warn "Use of uninitialized value of type { self.^name } in numeric context";
        0;
    }

    multi method Str (::?CLASS:D:) { "($!x, $!y)" }
    multi method Str (::?CLASS:U:) {
        warn "Use of uninitialized value of type { self.^name } in string context";
        '';
    }

    multi method list (::?CLASS:U:) { }
    multi method list (::?CLASS:D:) { $!x, $!y }

    multi method gist (::?CLASS:U:) { "({ self.^name })" }
    multi method gist (::?CLASS:D:) { "($!x, $!y)" }

    multi method raku (::?CLASS:U:) { 'Pop::Point' }
    multi method raku (::?CLASS:D:) {
        nqp::box_s(
            nqp::concat(
                'Pop::Point.new(',
                nqp::concat(
                    $!x,
                    nqp::concat(
                        ', ',
                        nqp::concat( $!y, ')' ),
                    ),
                ),
            ),
            ValueObjAt
        )
    }

    multi method WHICH(::?CLASS:D:) {
        nqp::box_s(
            nqp::concat(
                'Pop::Point|',
                nqp::concat(
                    $!x,
                    nqp::concat('|', $!y),
                ),
            ),
            ValueObjAt
        )
    }

    method !SET-SELF ( Num() \x, Num() \y ) {
        $!x = x;
        $!y = y;
        self
    }

    multi method ACCEPTS ( Numeric \other ) { $.length ~~ other }

    {
        use Pop::SDL;
        multi method sdl ( ::?CLASS:U: ) { SDL::Point }
        multi method sdl ( ::?CLASS:D: ) { SDL::Point.new: $!x, $!y }
    }

    multi submethod COERCE ( Real $n ) {
        self.new: $n, $n;
    }

    multi submethod COERCE ( @ ( Real $x, Real $y ) ) {
        self.new: $x, $y;
    }
}

multi infix:<cmp> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.length cmp b.length
}

multi infix:<+> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.add(b)
}

multi infix:<+> (Pop::Point \a, ( Real \x, Real \y ) ) is export(:operators) {
    a.add(x, y)
}

multi infix:<+> (Pop::Point \a, Real \b) is export(:operators) {
    a.add(b)
}

multi infix:<-> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.sub(b)
}

multi infix:<-> (Pop::Point \a, ( Real \x, Real \y ) ) is export(:operators) {
    a.sub(x, y)
}

multi infix:<-> (Pop::Point \a, Real \b) is export(:operators) {
    a.sub(b)
}

multi infix:<*> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.mul(b)
}

multi infix:<*> (Pop::Point \a, ( Real \x, Real \y ) ) is export(:operators) {
    a.mul(x, y)
}

multi infix:<*> (Pop::Point \a, Real \b) is export(:operators) {
    a.mul(b)
}

multi infix:</> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.div(b)
}

multi infix:</> (Pop::Point \a, ( Real \x, Real \y ) ) is export(:operators) {
    a.div(x, y)
}

multi infix:</> (Pop::Point \a, Real \b) is export(:operators) {
    a.div(b)
}

#| The sign of the 2D cross product tells you whether the second vector is on
#| the left or right side of the first vector (the direction of the first
#| vector being front). The absolute value of the 2D cross product is the sine
#| of the angle in between the two vectors, so taking the arc sine of it would
#| give you the angle in radians.
multi infix:<⨯> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.cross(b)
}

multi infix:<⋅> (Pop::Point \a, Pop::Point \b) is export(:operators) {
    a.dot(b)
}
