unit class Pop::Surface:ver<0.0.2>:auth<zef:jjatria>;

use Pop::SDL;

has SDL::Surface $.surface handles ( width => 'w', height => 'h' );
