class Pop::Rect {
    use Pop::Point;

    has Pop::Point $.xy handles < x y >;
    has Pop::Point $.wh handles ( w => 'x', h => 'y' );

    # Zero value constructor
    multi method zero {
        self.bless: xy => Pop::Point.zero, wh => Pop::Point.zero
    }

    # Constructor for rectangles at origin: takes width and height
    multi method new ( Real \w, Real \h = w ) {
        self.bless: xy => Pop::Point.zero, wh => Pop::Point.new( w, h )
    }

    # Constructor for arbitrary rectangles
    multi method new ( Real \x, Real \y, Real \w, Real \h ) {
        self.bless: xy => Pop::Point.new( x, y ), wh => Pop::Point.new( w, h )
    }

    method area { $.w * $.h }

    method round {
        self.bless: xy => $!xy.round, wh => $!wh.round
    }

    method floor {
        self.bless: xy => $!xy.floor, wh => $!wh.floor
    }

    method ceiling {
        self.bless: xy => $!xy.ceiling, wh => $!wh.ceiling
    }

    multi method collide ( Pop::Rect:D \o --> Bool ) {
           o.x <= ( $.x + $.w ) && $.x <= ( o.x + o.w )
        && o.y <= ( $.y + $.h ) && $.y <= ( o.y + o.h )
    }

    multi method collide ( Pop::Point:D \o --> Bool ) {
        $.x <= o.x < ( $.x + $.w ) && $.y <= o.y < ( $.y + $.h )
    }

    # Move the position of a rectangle
    multi method translate ( Real $x, Real $y = $x, Bool() :$absolute ) {
        return self.bless: xy => Pop::Point.new( $x, $y ), $!wh if $absolute;
        self.bless: xy => $!xy.add( $x, $y ), :$!wh
    }

    multi method translate ( Pop::Point:D $xy, Bool() :$absolute ) {
        return self.bless: :$xy, :$!wh if $absolute;
        self.bless: xy => $!xy.add( $xy ), :$!wh
    }

    # Change the size of a rectangle
    multi method resize ( Real $w, Real $h = $w, Bool() :$absolute ) {
        return self.bless: :$!xy, wh => Pop::Point.new( $w, $h ) if $absolute;
        self.bless: :$!xy, wh => $!wh.add( $w, $h )
    }

    multi method resize ( Pop::Point:D $wh, Bool() :$absolute ) {
        return self.bless: :$!xy, :$wh if $absolute;
        self.bless: :$!xy, wh => $!wh.add( $wh )
    }

    method Bool { self.defined && ( ?$!xy || ?$!wh ) }

    multi method Str (::?CLASS:D:) { '(' ~ join(', ', |self) ~ ')' }
    multi method Str (::?CLASS:U:) {
        warn "Use of uninitialized value of type { self.^name } in string context";
        '';
    }

    multi method list (::?CLASS:U:) { }
    multi method list (::?CLASS:D:) { |$!xy, |$!wh }

    multi method gist (::?CLASS:U:) { "({ self.^name })" }
    multi method gist (::?CLASS:D:) { '(' ~ join(', ', |self) ~ ')' }

    multi method raku (::?CLASS:U:) { 'Pop::Point' }
    multi method raku (::?CLASS:D:) { "Pop::Rect.new" ~ self }

    multi method WHICH(::?CLASS:D:) { ValueObjAt.new("Pop::Rect|$.x|$.y|$.w|$.h") }

    {
        use Pop::SDL;
        multi method sdl ( ::?CLASS:U: ) { SDL::Rect }
        multi method sdl ( ::?CLASS:D: ) {
            SDL::Rect.new: |$!xy.floor, |$!wh.floor
        }
    }

    multi submethod COERCE ( Real $n ) {
        self.new: $n
    }

    multi submethod COERCE ( @ ( Real $w, Real $h ) ) {
        self.new: $w, $h
    }

    multi submethod COERCE ( @ ( Pop::Point $xy, Pop::Point $wh ) ) {
        self.new: |$xy, |$wh
    }

    multi submethod COERCE ( @ ( Real $x, Real $y, Real $w, Real $h ) ) {
        self.new: $x, $y, $w, $h
    }

    multi submethod COERCE ( @ ( @ ( Real $x, Real $y ), @ ( Real $w, Real $h ) ) ) {
        self.new: $x, $y, $w, $h
    }
}
