#| The main entity registry, inspired by https://github.com/skypjack/entt
unit class Pop::Entities:ver<0.0.2>:auth<zef:jjatria>;

my constant SPARSE     = 0;
my constant DENSE      = 1;
my constant COMPONENTS = 2;

# Entity GUIDs are 32 bit integers:
# * 12 bits used for the entity version (used for recycing entities)
# * 20 bits used for the entity number
my constant ENTITY-MASK  = 0xFFFFF; # Used to convert GUIDs to entity numbers
my constant VERSION-MASK = 0xFFF;   # Used to convert GUIDs to entity versions
my constant ENTITY-SHIFT = 20;      # The size of the entity number within a GUID

# Keys in this hash are component type names (ie. the result of .^name),
# and values are sparse sets of entities that "have" that component.
my %COMPONENTS{Mu};

# Parameters used for recycling entity GUIDs
# See https://skypjack.github.io/2019-05-06-ecs-baf-part-3
my @ENTITIES;
my $AVAILABLE is default(ENTITY-MASK);

my %VIEW-CACHE;

# Indexing on the component registry is done using type objects
# If we get a role, we use its punned class in order to avoid
# missing reads later.
my &type = -> $_ { .HOW ~~ Metamodel::ClassHOW ?? .WHAT !! .^pun }

# Entity "methods"
my &version     = { $^entity +> ENTITY-SHIFT }
my &entity      = { $^entity +& ENTITY-MASK }
my &is-null     = { .&entity == ENTITY-MASK }
my &format      = { .&version.fmt('%012b') ~ ':' ~ .&entity.fmt('%020b') }
my &add-version = { $^e +| ( @ENTITIES[$^e].&version +< ENTITY-SHIFT ) }

method new {
    die "Pop::Entities is a Singleton, there's no need to create an instance"
}

method created ( --> Int ) { @ENTITIES.elems }

# Get the number of created entities that are still valid; that is, that have
# not been deleted.
method alive ( --> Int ) {
    my $size = @ENTITIES.elems;
    my $current = $AVAILABLE;

    while $current.&entity != ENTITY-MASK {
        $size--;
        $current = @ENTITIES[ $current +& ENTITY-MASK ];
    }

    $size;
}

# Reset the registry internal storage. All entities will be deleted, and all
# entity IDs will be made available.
method clear ( --> Nil ) {
    note "CLEAR" if %*ENV<POP_ENTITIES_DEBUG>;
    %COMPONENTS = ();
    %VIEW-CACHE = ();
    @ENTITIES   = ();
    $AVAILABLE  = Nil;
}

method !generate-guid ( --> int32 ) {
    die 'Exceeded maximum number of entities' if @ENTITIES.elems >= ENTITY-MASK - 1;
    my int32 $guid = @ENTITIES.elems;
    @ENTITIES.push: $guid;
    $guid;
}

method !recycle-guid ( --> int32 ) {
    die 'Cannot recycle GUID if none has been released' without $AVAILABLE;

    my int32 $current = $AVAILABLE;
    my $version = @ENTITIES[$current] +& ( VERSION-MASK +< ENTITY-SHIFT );

    $AVAILABLE = @ENTITIES[$current].&entity;
    @ENTITIES[$current] = $current +| $version;
}

# Create a new entity
method create ( *@components --> Int ) {
    my $guid = $AVAILABLE.&is-null ?? self!generate-guid !! self!recycle-guid;
    note "CREATE: { $guid.&format }" if %*ENV<POP_ENTITIES_DEBUG>;
    $.add: $guid, $_ for @components;
    $guid;
}

method check ( Int() $guid, $c --> Bool() ) {
    my $e = $guid.&entity;
    try {
        ( .[ DENSE; .[ SPARSE; $e ] ] // $e + 1 ) == $e
    } with %COMPONENTS{$c.&type}
}

# Add or replace a component for an entity
multi method add ( Int() $guid, $c --> Nil ) {
    my $e = $guid.&entity;
    my $type = $c.&type;

    note "ADD:    { $c.^name } => { $guid.&format }" if %*ENV<POP_ENTITIES_DEBUG>;

    #                        SPARSE  DENSE   COMPONENTS
    #                              \   |    /
    given %COMPONENTS{$type} //= [ [], [], [] ] {
        if $.check: $guid, $c { # Replace
            note "ADD:    Replace" if %*ENV<POP_ENTITIES_DEBUG>;
            .[ COMPONENTS; .[ SPARSE; $e ] ] = $c;
        }
        else { # Add
            note "ADD:    Add" if %*ENV<POP_ENTITIES_DEBUG>;
            .[COMPONENTS].push: $c;
            .[DENSE].push: $e;

            .[ SPARSE; $e ] = .[DENSE].elems - 1;
        }
    }

    # Adding a component invalidates any cached view that uses it
    %VIEW-CACHE{$_}:delete
        for %VIEW-CACHE.keys.grep: *.contains("|{ $type.^name }|");

    if %*ENV<POP_ENTITIES_DEBUG> {
        note 'ADD:    Components -----';
        for %COMPONENTS.kv -> $k, @v {
            note 'ADD:    ' ~ $k.^name ~ ' -----';
            note 'ADD:    ' ~ .raku for @v;
        }
        note 'ADD:    ----------------'
    };
}

method ^dump-entities ( *@components --> Nil ) {
    for @components {
        note "Dump for { .^name } component";
        with %COMPONENTS{$_} {
            for 0 .. .[SPARSE].elems -> $i {
                FIRST note 'SPARSE DENSE WHERE        COMPONENT';

                my $c = .[COMPONENTS; $i];

                note sprintf '%6s %5s %12X %s',
                    .[SPARSE; $i] // '---',
                    .[DENSE; $i] // '---',
                    $c.WHERE,
                    $c.gist // '---';
            }
        }
    }
}

# Add or replace a set of components for an entity
multi method add ( $guid, *@c where *.elems > 1 --> Nil ) {
    $.add: $guid, $_ for @c
}

# Get a component for an entity
multi method get ( Int() $guid, $c, :$check = True ) {
    my $e = $guid.&entity;
    return $c.WHAT if $check && !$.check: $guid, $c;

    try { .[ COMPONENTS; .[ SPARSE; $e ] ] } // $c.WHAT
        with %COMPONENTS{$c.&type};
}

# Get a set of components for an entity
multi method get ( $guid, *@c where *.elems > 1, |c ) {
    @c.map: { $.get: $guid, $_, |c }
}

# Remove an entity and all its components
multi method delete ( Int() $guid --> Nil ) {
    note "DELETE: { $guid.&format }" if %*ENV<POP_ENTITIES_DEBUG>;

    $.delete( $guid, $_ ) for %COMPONENTS.keys;

    # We mark an entity as available by splitting the entity and the version
    # and storing the incremented version only in the entities list, and the
    # available entity ID in $AVAILABLE

    my $entity  = $guid.&entity;
    my $version = $guid.&version + 1;

    @ENTITIES[$entity] = $AVAILABLE +| ( $version +< ENTITY-SHIFT );
    $AVAILABLE = $entity;
}

# Remove a component from an entity
multi method delete ( Int() $guid, $c --> Nil ) {
    return unless $.check: $guid, $c;
    note "DELETE: { $c.^name } from { $guid.&format }" if %*ENV<POP_ENTITIES_DEBUG>;
    my $e = $guid.&entity;
    my $type = $c.&type;

    with %COMPONENTS{$type} {
        my ( $i, $j ) = ( .[ SPARSE; $e ], .[DENSE].elems - 1 );

        for .[DENSE], .[COMPONENTS] {
            .[ $i, $j ] = .[ $j, $i ];
            .pop;
        }

        try { .[ SPARSE; .[DENSE; $i] ] = $i }

        if %*ENV<POP_ENTITIES_DEBUG> {
            note 'DELETE: sparse     = ' ~ .[SPARSE].raku;
            note 'DELETE: dense      = ' ~ .[DENSE].raku;
            note 'DELETE: components = ' ~ .[COMPONENTS].raku;
        }
    }

    # Deleting a component invalidates any cached view that uses it
    %VIEW-CACHE{$_}:delete
        for %VIEW-CACHE.keys.grep: *.contains("|{ $type.^name }|");
}

# Delete a set of components from an entity
multi method delete ( $guid, *@c where *.elems > 1 --> Nil ) {
    @c.map: { $.delete: $guid, $_ }
}

my class View {
    has @!components is built is required;
    has $!view       is built;

    method iterator { $!view».key.iterator }

    multi method each ( &code where *.cando: \( Int, |@!components ) --> Nil ) {
        code( .key, |.value ) for |$!view;
    }

    multi method each ( &code where *.cando: @!components.Capture --> Nil ) {
        # note 'Iterating with arity';
        code( |.value ) for |$!view;
    }

    multi method each ( &code --> Nil ) {
        die Q:c:to<FATAL>.chomp;
            The block passed to this view's .each could not be used. To fix this,
            please use a block that can be called with either

                { @!components.Capture.gist }

            to receive the components, or

                { ( Int, |@!components ).Capture.gist }

            to receive the entity and its components
            FATAL
    }
}

# Checks if an entity identifier refers to a valid entity; that is, one that
# has been created and not deleted.
method valid ( Int() $guid --> Bool ) {
    my $pos = $guid.&entity;
    $pos < @ENTITIES.elems && @ENTITIES[$pos] == $guid
}

# Return a view for all entities
multi method view ( Whatever --> View ) {
    note 'VIEW:   Getting a view for all components'
        if %*ENV<POP_ENTITIES_DEBUG>;

    # The view of all entities is never cached
    return View.new(
        components => Empty,
        view => eager @ENTITIES.grep({ $.valid: $_ }).map: {
            .&entity.&add-version => Empty;
        }
    )
}

# Return a view for entities that have the specified set of components
multi method view ( $c --> View ) {
    note "VIEW:   Getting a view for single component { $c.^name }"
        if %*ENV<POP_ENTITIES_DEBUG>;

    my $type = $c.&type;
    %VIEW-CACHE{ "|$type.^name()|" } //= do {
        my $base  := %COMPONENTS{$type};
        my $comps := $base[COMPONENTS];

        View.new(
            components => ( $c, ),
            view => eager $base[DENSE].kv.map: -> $i, $e {
                $e.&add-version => ( $comps[$i]<>, );
            },
        )
    }
}

# Return a view for entities that have the specified set of components
multi method view ( *@c where *.elems > 1 --> View ) {
    my @components = @c.map: { ( .defined ?? .WHAT !! $_ ).&type }
    note 'VIEW:   Getting a view for multiple components: '
         ~ join ', ', @components.map: *.^name if %*ENV<POP_ENTITIES_DEBUG>;

    %VIEW-CACHE{ "|@components.map(*.^name).join('|')|" } //= do {
        my ( $short, @rest) = @components.sort: { %COMPONENTS{$_}.[DENSE].elems }
        my $base  := %COMPONENTS{$short};
        my $comps := $base[COMPONENTS];

        my @view = eager gather for $base[DENSE].kv -> $i, $e {
            my $guid = $e.&add-version;

            # TODO: This can probably be simplified
            my $not;
            for @rest {
                unless $.check: $guid, $_ {
                    $not = True;
                    last;
                }
            }

            next if $not;

            take $guid => eager @components.map: {
                when $short { $comps[$i]<> }
                default { $.get( $guid, $_, :!check )<> }
            }
        }

        View.new( :@components, :@view )
    }
}
