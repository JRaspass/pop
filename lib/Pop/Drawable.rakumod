unit role Pop::Drawable:ver<0.0.2>:auth<zef:jjatria>;

use Pop::Color;
use Pop::SDL;

has SDL::Texture $.texture;
has               $.width;
has               $.height;

method clip { ... }

multi method blend-mode ( Str $_ --> Nil ) {
    my $mode = SDL::BlendMode::{"BLENDMODE_{ .uc }"}
        or die "No such blend mode: '$_'";

    SDL::SetTextureBlendMode( $!texture, $mode )
        and die "Unable to set blend mode: { SDL::GetError() }";
}

multi method blend-mode ( --> Str ) {
    my $mode;
    SDL::GetTextureBlendMode ($!texture, $mode )
        and die "Unable to get blend mode: { SDL::GetError() }";
    SDL::BlendMode($mode).subst('BLENDMODE_').lc;
}

multi method color-mod ( Pop::Color $col --> Nil ) {
    SDL::SetTextureColorMod( $!texture, |$col.rgb )
        and die "Unable to set color mod: { SDL::GetError() }";
}

multi method color-mod ( $r, $g, $b --> Nil ) {
    SDL::SetTextureColorMod( $!texture, $r, $g, $b )
        and die "Unable to set color mod: { SDL::GetError() }";
}

multi method color-mod ( --> Pop::Color ) {
    my ( $r, $g, $b );
    SDL::GetTextureColorMod( $!texture, $r, $g, $b )
        and die "Unable to get texture color mod: { SDL::GetError() }";
    Pop::Color.new( $r, $g, $b )
}
