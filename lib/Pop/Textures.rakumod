use Pop::Singleton;
unit class Pop::Textures:ver<0.0.2>:auth<zef:jjatria> is Singleton;

use Pop::Point;
use Pop::Texture;
use Pop::Graphics;
use Pop::SDL;

has Pop::Texture %!textures;

# Loads an image either from the cache or the filesystem and creates a
# texture from it.
#
# If the image did not exist in the cache and had to be loaded from the
# filesystem, it will be destroying as soon as the texture has been created.
# To prevent this, load the image manually with `Pop::Images.load`. Note that
# the key used to query the image cache will be the same key that has been
# specified for the texture.
#
# The value of the `:cache` flag applies to the texture being created, not
# any underlying data structures used as part of its creation process.
method load (
    IO()  $path,
    Str() $key   = $path,
    Bool :$cache = True
    --> Pop::Texture
) {
    # Image has been loaded before and can be reused
    return $.create: $_, $key, :$cache  with Pop::Images.get: $key;

    # Image has not been loaded. We load it and destroy
    my $image = Pop::Images.load: $path, $key, :!cache;

    $.create: $image, $key, :$cache;
}

multi method create (
    Pop::Surface $surface,
    Str()        $key?,
    --> Pop::Texture
) {
    my $texture = SDL::CreateTextureFromSurface(
        Pop::Graphics.renderer,
        $surface.surface,
    ) or die "Error creating texture from surface: { SDL::GetError() }";

    my $result = Pop::Texture.new:
        :$texture, width => $surface.width, height => $surface.height;

    %!textures{$key} = $result if $key;

    $result;
}

multi method create (
    Pop::Point:D() $dims,
    Str()          $key?,
                  :$format is copy,
                  :$access is copy,
    --> Pop::Texture
) {
    $format //= SDL::GetWindowPixelFormat( Pop::Graphics.window )
        or die "Error getting pixel format from window: { SDL::GetError() }";

    $access //= SDL::TEXTUREACCESS_TARGET;

    my $texture = SDL::CreateTexture(
        Pop::Graphics.renderer,
        $format,
        $access,
        |$dims».Int,
    ) or die "Error creating texture: { SDL::GetError() }";

    my $result = Pop::Texture.new:
        :$texture, width => $dims.x, height => $dims.y;

    %!textures{$key} = $result if $key;

    $result;
}


# Fetches a loaded texture from cache by te specified key. If no texture by
# that key has been loaded, a type object is returned. This function never
# reads from the filesystem.
method get ( Str:D() $key --> Pop::Texture ) { %!textures{$key} // Pop::Texture }

# Unloads a texture from memory
multi method destroy ( Str:D() $key --> Nil ) {
    with %!textures{$key}:delete {
        note "Unloading $key texture...";
        .destroy;
    }
    else {
        die "No texture with that key: $key";
    }
}

# Destroy all cached textures as well as all internal data structures
multi method destroy ( --> Nil ) {
    for %!textures.keys {
        FIRST note 'Unloading textures';
        .destroy with %!textures{$_}:delete;
    }
}
