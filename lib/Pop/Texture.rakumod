use Pop::Drawable;
unit class Pop::Texture:ver<0.0.2>:auth<zef:jjatria> does Pop::Drawable;

use Pop::Rect;
use Pop::Sprite;

has Int $.tile-width  = 1;
has Int $.tile-height = 1;

has Pop::Rect $!clip; method clip { $!clip } # Not available in constructor

has Capture %!sprites;

# All textures define a 'default' sprite, with a single frame encompassing the
# entire texture.
submethod TWEAK {
    $!clip = Pop::Rect.new( 0, 0, $!width, $!height );
    %!sprites<default> = \( frames => $!clip.clone );
}

# Creates a sprite from this texture. A sprite can be understood as a set of
# rules to move a source rectangle over this texture. For animated sprites,
# these moves represent individual frames.
#
# If a name is given to this sprite as the first positional argument, the
# rules are cached in the texture as a recipe to generate a copy of this
# sprite if needed.
#
method make-sprite (
    ::?CLASS:D:
    Str()   $name?,
           :$x       = 0,
           :$y       = 0,
           :$fps     = 0,
    Bool() :$loop    = False,
           :$frame   = [ 0, 0 ],
           :@frames is copy,
        :w(:$width)  = $!tile-width  // $!width,
        :h(:$height) = $!tile-height // $!height,
    --> Pop::Sprite
) {
    @frames ||= $frame;
    @frames .= map: {
        Pop::Rect.new(
            $x + .[0] *  $width, $y + .[1] *  $height,
                 .[2] // $width,      .[3] // $height,
        )
    }

    my $sprite = \(
        :$fps, :$loop, :$width, :$height, :@frames,
        rect => Pop::Rect.new( $x, $y, $width, $height ),
    );

    %!sprites{$_} = $sprite with $name;

    Pop::Sprite.new: :$!texture, |$sprite;
}

# Returns a new copy of the sprite specified by name.
method get-sprite ( ::?CLASS:D: Str:D() $name --> Pop::Sprite ) {
    my \c = %!sprites{$name}
        // die "No sprite with this key has been set: $name";

    Pop::Sprite.new: :$!texture, |c;
}

multi method destroy ( --> Nil ) {
    use Pop::SDL;
    SDL::DestroyTexture($_) with $!texture;
    $!texture = Nil;
    self.destroy-sprites;
}

# Deletes any sprite recipe that has been stored with the specified name.
# Deleting a sprite that has not been set with make-sprite is not an error.
multi method destroy ( Str:D() $name --> Nil ) { %!sprites{$name}:delete }

method destroy-sprites ( --> Nil ) { %!sprites = () }
