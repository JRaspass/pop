unit module SDL:ver<0.0.2>:auth<zef:jjatria>;

use NativeCall;

my constant LIB = 'SDL2';

our constant INIT_TIMER          = 0x00000001;
our constant INIT_AUDIO          = 0x00000010;
our constant INIT_VIDEO          = 0x00000020; # INIT_VIDEO implies INIT_EVENTS
our constant INIT_JOYSTICK       = 0x00000200; # INIT_JOYSTICK implies INIT_EVENTS
our constant INIT_HAPTIC         = 0x00001000;
our constant INIT_GAMECONTROLLER = 0x00002000; # INIT_GAMECONTROLLER implies INIT_JOYSTICK
our constant INIT_EVENTS         = 0x00004000;
our constant INIT_SENSOR         = 0x00008000;
our constant INIT_NOPARACHUTE    = 0x00100000; # compatibility; this flag is ignored
our constant INIT_EVERYTHING     = (
    INIT_TIMER    +| INIT_AUDIO  +| INIT_VIDEO          +| INIT_EVENTS +|
    INIT_JOYSTICK +| INIT_HAPTIC +| INIT_GAMECONTROLLER +| INIT_SENSOR
);

our constant HINT_ACCELEROMETER_AS_JOYSTICK                = 'SDL_ACCELEROMETER_AS_JOYSTICK';
our constant HINT_ALLOW_ALT_TAB_WHILE_GRABBED              = 'SDL_ALLOW_ALT_TAB_WHILE_GRABBED';
our constant HINT_ALLOW_TOPMOST                            = 'SDL_ALLOW_TOPMOST';
our constant HINT_ANDROID_APK_EXPANSION_MAIN_FILE_VERSION  = 'SDL_ANDROID_APK_EXPANSION_MAIN_FILE_VERSION';
our constant HINT_ANDROID_APK_EXPANSION_PATCH_FILE_VERSION = 'SDL_ANDROID_APK_EXPANSION_PATCH_FILE_VERSION';
our constant HINT_ANDROID_BLOCK_ON_PAUSE                   = 'SDL_ANDROID_BLOCK_ON_PAUSE';
our constant HINT_ANDROID_BLOCK_ON_PAUSE_PAUSEAUDIO        = 'SDL_ANDROID_BLOCK_ON_PAUSE_PAUSEAUDIO';
our constant HINT_ANDROID_TRAP_BACK_BUTTON                 = 'SDL_ANDROID_TRAP_BACK_BUTTON';
our constant HINT_APPLE_TV_CONTROLLER_UI_EVENTS            = 'SDL_APPLE_TV_CONTROLLER_UI_EVENTS';
our constant HINT_APPLE_TV_REMOTE_ALLOW_ROTATION           = 'SDL_APPLE_TV_REMOTE_ALLOW_ROTATION';
our constant HINT_AUDIO_CATEGORY                           = 'SDL_AUDIO_CATEGORY';
our constant HINT_AUDIO_DEVICE_APP_NAME                    = 'SDL_AUDIO_DEVICE_APP_NAME';
our constant HINT_AUDIO_DEVICE_STREAM_NAME                 = 'SDL_AUDIO_DEVICE_STREAM_NAME';
our constant HINT_AUDIO_RESAMPLING_MODE                    = 'SDL_AUDIO_RESAMPLING_MODE';
our constant HINT_AUTO_UPDATE_JOYSTICKS                    = 'SDL_AUTO_UPDATE_JOYSTICKS';
our constant HINT_AUTO_UPDATE_SENSORS                      = 'SDL_AUTO_UPDATE_SENSORS';
our constant HINT_BMP_SAVE_LEGACY_FORMAT                   = 'SDL_BMP_SAVE_LEGACY_FORMAT';
our constant HINT_DISPLAY_USABLE_BOUNDS                    = 'SDL_DISPLAY_USABLE_BOUNDS';
our constant HINT_EMSCRIPTEN_ASYNCIFY                      = 'SDL_EMSCRIPTEN_ASYNCIFY';
our constant HINT_EMSCRIPTEN_KEYBOARD_ELEMENT              = 'SDL_EMSCRIPTEN_KEYBOARD_ELEMENT';
our constant HINT_ENABLE_STEAM_CONTROLLERS                 = 'SDL_ENABLE_STEAM_CONTROLLERS';
our constant HINT_EVENT_LOGGING                            = 'SDL_EVENT_LOGGING';
our constant HINT_FRAMEBUFFER_ACCELERATION                 = 'SDL_FRAMEBUFFER_ACCELERATION';
our constant HINT_GAMECONTROLLERCONFIG                     = 'SDL_GAMECONTROLLERCONFIG';
our constant HINT_GAMECONTROLLERCONFIG_FILE                = 'SDL_GAMECONTROLLERCONFIG_FILE';
our constant HINT_GAMECONTROLLERTYPE                       = 'SDL_GAMECONTROLLERTYPE';
our constant HINT_GAMECONTROLLER_IGNORE_DEVICES            = 'SDL_GAMECONTROLLER_IGNORE_DEVICES';
our constant HINT_GAMECONTROLLER_IGNORE_DEVICES_EXCEPT     = 'SDL_GAMECONTROLLER_IGNORE_DEVICES_EXCEPT';
our constant HINT_GAMECONTROLLER_USE_BUTTON_LABELS         = 'SDL_GAMECONTROLLER_USE_BUTTON_LABELS';
our constant HINT_GRAB_KEYBOARD                            = 'SDL_GRAB_KEYBOARD';
our constant HINT_IDLE_TIMER_DISABLED                      = 'SDL_IOS_IDLE_TIMER_DISABLED';
our constant HINT_IME_INTERNAL_EDITING                     = 'SDL_IME_INTERNAL_EDITING';
our constant HINT_IOS_HIDE_HOME_INDICATOR                  = 'SDL_IOS_HIDE_HOME_INDICATOR';
our constant HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS         = 'SDL_JOYSTICK_ALLOW_BACKGROUND_EVENTS';
our constant HINT_JOYSTICK_HIDAPI                          = 'SDL_JOYSTICK_HIDAPI';
our constant HINT_JOYSTICK_HIDAPI_CORRELATE_XINPUT         = 'SDL_JOYSTICK_HIDAPI_CORRELATE_XINPUT';
our constant HINT_JOYSTICK_HIDAPI_GAMECUBE                 = 'SDL_JOYSTICK_HIDAPI_GAMECUBE';
our constant HINT_JOYSTICK_HIDAPI_JOY_CONS                 = 'SDL_JOYSTICK_HIDAPI_JOY_CONS';
our constant HINT_JOYSTICK_HIDAPI_PS4                      = 'SDL_JOYSTICK_HIDAPI_PS4';
our constant HINT_JOYSTICK_HIDAPI_PS4_RUMBLE               = 'SDL_JOYSTICK_HIDAPI_PS4_RUMBLE';
our constant HINT_JOYSTICK_HIDAPI_PS5                      = 'SDL_JOYSTICK_HIDAPI_PS5';
our constant HINT_JOYSTICK_HIDAPI_PS5_PLAYER_LED           = 'SDL_JOYSTICK_HIDAPI_PS5_PLAYER_LED';
our constant HINT_JOYSTICK_HIDAPI_PS5_RUMBLE               = 'SDL_JOYSTICK_HIDAPI_PS5_RUMBLE';
our constant HINT_JOYSTICK_HIDAPI_STADIA                   = 'SDL_JOYSTICK_HIDAPI_STADIA';
our constant HINT_JOYSTICK_HIDAPI_STEAM                    = 'SDL_JOYSTICK_HIDAPI_STEAM';
our constant HINT_JOYSTICK_HIDAPI_SWITCH                   = 'SDL_JOYSTICK_HIDAPI_SWITCH';
our constant HINT_JOYSTICK_HIDAPI_SWITCH_HOME_LED          = 'SDL_JOYSTICK_HIDAPI_SWITCH_HOME_LED';
our constant HINT_JOYSTICK_HIDAPI_XBOX                     = 'SDL_JOYSTICK_HIDAPI_XBOX';
our constant HINT_JOYSTICK_RAWINPUT                        = 'SDL_JOYSTICK_RAWINPUT';
our constant HINT_JOYSTICK_THREAD                          = 'SDL_JOYSTICK_THREAD';
our constant HINT_LINUX_JOYSTICK_DEADZONES                 = 'SDL_LINUX_JOYSTICK_DEADZONES';
our constant HINT_MAC_BACKGROUND_APP                       = 'SDL_MAC_BACKGROUND_APP';
our constant HINT_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK       = 'SDL_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK';
our constant HINT_MOUSE_DOUBLE_CLICK_RADIUS                = 'SDL_MOUSE_DOUBLE_CLICK_RADIUS';
our constant HINT_MOUSE_DOUBLE_CLICK_TIME                  = 'SDL_MOUSE_DOUBLE_CLICK_TIME';
our constant HINT_MOUSE_FOCUS_CLICKTHROUGH                 = 'SDL_MOUSE_FOCUS_CLICKTHROUGH';
our constant HINT_MOUSE_NORMAL_SPEED_SCALE                 = 'SDL_MOUSE_NORMAL_SPEED_SCALE';
our constant HINT_MOUSE_RELATIVE_MODE_WARP                 = 'SDL_MOUSE_RELATIVE_MODE_WARP';
our constant HINT_MOUSE_RELATIVE_SCALING                   = 'SDL_MOUSE_RELATIVE_SCALING';
our constant HINT_MOUSE_RELATIVE_SPEED_SCALE               = 'SDL_MOUSE_RELATIVE_SPEED_SCALE';
our constant HINT_MOUSE_TOUCH_EVENTS                       = 'SDL_MOUSE_TOUCH_EVENTS';
our constant HINT_NO_SIGNAL_HANDLERS                       = 'SDL_NO_SIGNAL_HANDLERS';
our constant HINT_OPENGL_ES_DRIVER                         = 'SDL_OPENGL_ES_DRIVER';
our constant HINT_ORIENTATIONS                             = 'SDL_IOS_ORIENTATIONS';
our constant HINT_PREFERRED_LOCALES                        = 'SDL_PREFERRED_LOCALES';
our constant HINT_QTWAYLAND_CONTENT_ORIENTATION            = 'SDL_QTWAYLAND_CONTENT_ORIENTATION';
our constant HINT_QTWAYLAND_WINDOW_FLAGS                   = 'SDL_QTWAYLAND_WINDOW_FLAGS';
our constant HINT_RENDER_BATCHING                          = 'SDL_RENDER_BATCHING';
our constant HINT_RENDER_DIRECT3D11_DEBUG                  = 'SDL_RENDER_DIRECT3D11_DEBUG';
our constant HINT_RENDER_DIRECT3D_THREADSAFE               = 'SDL_RENDER_DIRECT3D_THREADSAFE';
our constant HINT_RENDER_DRIVER                            = 'SDL_RENDER_DRIVER';
our constant HINT_RENDER_LOGICAL_SIZE_MODE                 = 'SDL_RENDER_LOGICAL_SIZE_MODE';
our constant HINT_RENDER_OPENGL_SHADERS                    = 'SDL_RENDER_OPENGL_SHADERS';
our constant HINT_RENDER_SCALE_QUALITY                     = 'SDL_RENDER_SCALE_QUALITY';
our constant HINT_RENDER_VSYNC                             = 'SDL_RENDER_VSYNC';
our constant HINT_RETURN_KEY_HIDES_IME                     = 'SDL_RETURN_KEY_HIDES_IME';
our constant HINT_RPI_VIDEO_LAYER                          = 'SDL_RPI_VIDEO_LAYER';
our constant HINT_THREAD_FORCE_REALTIME_TIME_CRITICAL      = 'SDL_THREAD_FORCE_REALTIME_TIME_CRITICAL';
our constant HINT_THREAD_PRIORITY_POLICY                   = 'SDL_THREAD_PRIORITY_POLICY';
our constant HINT_THREAD_STACK_SIZE                        = 'SDL_THREAD_STACK_SIZE';
our constant HINT_TIMER_RESOLUTION                         = 'SDL_TIMER_RESOLUTION';
our constant HINT_TOUCH_MOUSE_EVENTS                       = 'SDL_TOUCH_MOUSE_EVENTS';
our constant HINT_TV_REMOTE_AS_JOYSTICK                    = 'SDL_TV_REMOTE_AS_JOYSTICK';
our constant HINT_VIDEO_ALLOW_SCREENSAVER                  = 'SDL_VIDEO_ALLOW_SCREENSAVER';
our constant HINT_VIDEO_DOUBLE_BUFFER                      = 'SDL_VIDEO_DOUBLE_BUFFER';
our constant HINT_VIDEO_EXTERNAL_CONTEXT                   = 'SDL_VIDEO_EXTERNAL_CONTEXT';
our constant HINT_VIDEO_HIGHDPI_DISABLED                   = 'SDL_VIDEO_HIGHDPI_DISABLED';
our constant HINT_VIDEO_MAC_FULLSCREEN_SPACES              = 'SDL_VIDEO_MAC_FULLSCREEN_SPACES';
our constant HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS             = 'SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS';
our constant HINT_VIDEO_WINDOW_SHARE_PIXEL_FORMAT          = 'SDL_VIDEO_WINDOW_SHARE_PIXEL_FORMAT';
our constant HINT_VIDEO_WIN_D3DCOMPILER                    = 'SDL_VIDEO_WIN_D3DCOMPILER';
our constant HINT_VIDEO_X11_FORCE_EGL                      = 'SDL_VIDEO_X11_FORCE_EGL';
our constant HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR       = 'SDL_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR';
our constant HINT_VIDEO_X11_NET_WM_PING                    = 'SDL_VIDEO_X11_NET_WM_PING';
our constant HINT_VIDEO_X11_WINDOW_VISUALID                = 'SDL_VIDEO_X11_WINDOW_VISUALID';
our constant HINT_VIDEO_X11_XINERAMA                       = 'SDL_VIDEO_X11_XINERAMA';
our constant HINT_VIDEO_X11_XRANDR                         = 'SDL_VIDEO_X11_XRANDR';
our constant HINT_VIDEO_X11_XVIDMODE                       = 'SDL_VIDEO_X11_XVIDMODE';
our constant HINT_WAVE_FACT_CHUNK                          = 'SDL_WAVE_FACT_CHUNK';
our constant HINT_WAVE_RIFF_CHUNK_SIZE                     = 'SDL_WAVE_RIFF_CHUNK_SIZE';
our constant HINT_WAVE_TRUNCATION                          = 'SDL_WAVE_TRUNCATION';
our constant HINT_WINDOWS_DISABLE_THREAD_NAMING            = 'SDL_WINDOWS_DISABLE_THREAD_NAMING';
our constant HINT_WINDOWS_ENABLE_MESSAGELOOP               = 'SDL_WINDOWS_ENABLE_MESSAGELOOP';
our constant HINT_WINDOWS_FORCE_MUTEX_CRITICAL_SECTIONS    = 'SDL_WINDOWS_FORCE_MUTEX_CRITICAL_SECTIONS';
our constant HINT_WINDOWS_FORCE_SEMAPHORE_KERNEL           = 'SDL_WINDOWS_FORCE_SEMAPHORE_KERNEL';
our constant HINT_WINDOWS_INTRESOURCE_ICON                 = 'SDL_WINDOWS_INTRESOURCE_ICON';
our constant HINT_WINDOWS_INTRESOURCE_ICON_SMALL           = 'SDL_WINDOWS_INTRESOURCE_ICON_SMALL';
our constant HINT_WINDOWS_NO_CLOSE_ON_ALT_F4               = 'SDL_WINDOWS_NO_CLOSE_ON_ALT_F4';
our constant HINT_WINDOWS_USE_D3D9EX                       = 'SDL_WINDOWS_USE_D3D9EX';
our constant HINT_WINDOW_FRAME_USABLE_WHILE_CURSOR_HIDDEN  = 'SDL_WINDOW_FRAME_USABLE_WHILE_CURSOR_HIDDEN';
our constant HINT_WINRT_HANDLE_BACK_BUTTON                 = 'SDL_WINRT_HANDLE_BACK_BUTTON';
our constant HINT_WINRT_PRIVACY_POLICY_LABEL               = 'SDL_WINRT_PRIVACY_POLICY_LABEL';
our constant HINT_WINRT_PRIVACY_POLICY_URL                 = 'SDL_WINRT_PRIVACY_POLICY_URL';
our constant HINT_XINPUT_ENABLED                           = 'SDL_XINPUT_ENABLED';
our constant HINT_XINPUT_USE_OLD_JOYSTICK_MAPPING          = 'SDL_XINPUT_USE_OLD_JOYSTICK_MAPPING';

our constant WINDOWPOS_UNDEFINED                           = 0x1FFF0000;
our constant WINDOWPOS_CENTERED                            = 0x2FFF0000;

our sub SetHint ( Str $name, Str $value )
    returns int32
    is symbol('SDL_SetHint')
    is native(LIB) {*}

our enum Scancode (
    SCANCODE_UNKNOWN            =>   0,

    # Usage page 0x07
    SCANCODE_A                  =>   4,
    SCANCODE_B                  =>   5,
    SCANCODE_C                  =>   6,
    SCANCODE_D                  =>   7,
    SCANCODE_E                  =>   8,
    SCANCODE_F                  =>   9,
    SCANCODE_G                  =>  10,
    SCANCODE_H                  =>  11,
    SCANCODE_I                  =>  12,
    SCANCODE_J                  =>  13,
    SCANCODE_K                  =>  14,
    SCANCODE_L                  =>  15,
    SCANCODE_M                  =>  16,
    SCANCODE_N                  =>  17,
    SCANCODE_O                  =>  18,
    SCANCODE_P                  =>  19,
    SCANCODE_Q                  =>  20,
    SCANCODE_R                  =>  21,
    SCANCODE_S                  =>  22,
    SCANCODE_T                  =>  23,
    SCANCODE_U                  =>  24,
    SCANCODE_V                  =>  25,
    SCANCODE_W                  =>  26,
    SCANCODE_X                  =>  27,
    SCANCODE_Y                  =>  28,
    SCANCODE_Z                  =>  29,

    SCANCODE_1                  =>  30,
    SCANCODE_2                  =>  31,
    SCANCODE_3                  =>  32,
    SCANCODE_4                  =>  33,
    SCANCODE_5                  =>  34,
    SCANCODE_6                  =>  35,
    SCANCODE_7                  =>  36,
    SCANCODE_8                  =>  37,
    SCANCODE_9                  =>  38,
    SCANCODE_0                  =>  39,

    SCANCODE_RETURN             =>  40,
    SCANCODE_ESCAPE             =>  41,
    SCANCODE_BACKSPACE          =>  42,
    SCANCODE_TAB                =>  43,
    SCANCODE_SPACE              =>  44,

    SCANCODE_MINUS              =>  45,
    SCANCODE_EQUALS             =>  46,
    SCANCODE_LEFTBRACKET        =>  47,
    SCANCODE_RIGHTBRACKET       =>  48,
    SCANCODE_BACKSLASH          =>  49,
    SCANCODE_NONUSHASH          =>  50,
    SCANCODE_SEMICOLON          =>  51,
    SCANCODE_APOSTROPHE         =>  52,
    SCANCODE_GRAVE              =>  53,
    SCANCODE_COMMA              =>  54,
    SCANCODE_PERIOD             =>  55,
    SCANCODE_SLASH              =>  56,

    SCANCODE_CAPSLOCK           =>  57,

    SCANCODE_F1                 =>  58,
    SCANCODE_F2                 =>  59,
    SCANCODE_F3                 =>  60,
    SCANCODE_F4                 =>  61,
    SCANCODE_F5                 =>  62,
    SCANCODE_F6                 =>  63,
    SCANCODE_F7                 =>  64,
    SCANCODE_F8                 =>  65,
    SCANCODE_F9                 =>  66,
    SCANCODE_F10                =>  67,
    SCANCODE_F11                =>  68,
    SCANCODE_F12                =>  69,

    SCANCODE_PRINTSCREEN        =>  70,
    SCANCODE_SCROLLLOCK         =>  71,
    SCANCODE_PAUSE              =>  72,
    SCANCODE_INSERT             =>  73, # insert on PC,
                                        # help on some Mac keyboards
                                        # (but does send code 73, not 117)
    SCANCODE_HOME               =>  74,
    SCANCODE_PAGEUP             =>  75,
    SCANCODE_DELETE             =>  76,
    SCANCODE_END                =>  77,
    SCANCODE_PAGEDOWN           =>  78,
    SCANCODE_RIGHT              =>  79,
    SCANCODE_LEFT               =>  80,
    SCANCODE_DOWN               =>  81,
    SCANCODE_UP                 =>  82,

    SCANCODE_NUMLOCKCLEAR       =>  83, # num lock on PC,
                                        # clear on Mac keyboards
    SCANCODE_KP_DIVIDE          =>  84,
    SCANCODE_KP_MULTIPLY        =>  85,
    SCANCODE_KP_MINUS           =>  86,
    SCANCODE_KP_PLUS            =>  87,
    SCANCODE_KP_ENTER           =>  88,
    SCANCODE_KP_1               =>  89,
    SCANCODE_KP_2               =>  90,
    SCANCODE_KP_3               =>  91,
    SCANCODE_KP_4               =>  92,
    SCANCODE_KP_5               =>  93,
    SCANCODE_KP_6               =>  94,
    SCANCODE_KP_7               =>  95,
    SCANCODE_KP_8               =>  96,
    SCANCODE_KP_9               =>  97,
    SCANCODE_KP_0               =>  98,
    SCANCODE_KP_PERIOD          =>  99,

    SCANCODE_NONUSBACKSLASH     => 100, # This is the additional key that
                                        # ISO keyboards have over ANSI
                                        # ones, located between left shift
                                        # and Y. Produces GRAVE ACCENT and
                                        # TILDE in a US or UK Mac layout,
                                        # REVERSE SOLIDUS (backslash) and
                                        # VERTICAL LINE in a US or UK
                                        # Windows layout, and LESS-THAN
                                        # SIGN and GREATER-THAN SIGN in a
                                        # Swiss German, German, or French
                                        # layout.
    SCANCODE_APPLICATION        => 101, # windows contextual menu, compose
    SCANCODE_POWER              => 102, # The USB document says this is a
                                        # status flag, not a physical key
                                        # - but some Mac keyboards do have
                                        # a power key.
    SCANCODE_KP_EQUALS          => 103,
    SCANCODE_F13                => 104,
    SCANCODE_F14                => 105,
    SCANCODE_F15                => 106,
    SCANCODE_F16                => 107,
    SCANCODE_F17                => 108,
    SCANCODE_F18                => 109,
    SCANCODE_F19                => 110,
    SCANCODE_F20                => 111,
    SCANCODE_F21                => 112,
    SCANCODE_F22                => 113,
    SCANCODE_F23                => 114,
    SCANCODE_F24                => 115,
    SCANCODE_EXECUTE            => 116,
    SCANCODE_HELP               => 117,
    SCANCODE_MENU               => 118,
    SCANCODE_SELECT             => 119,
    SCANCODE_STOP               => 120,
    SCANCODE_AGAIN              => 121, # redo
    SCANCODE_UNDO               => 122,
    SCANCODE_CUT                => 123,
    SCANCODE_COPY               => 124,
    SCANCODE_PASTE              => 125,
    SCANCODE_FIND               => 126,
    SCANCODE_MUTE               => 127,
    SCANCODE_VOLUMEUP           => 128,
    SCANCODE_VOLUMEDOWN         => 129,
    SCANCODE_KP_COMMA           => 133,
    SCANCODE_KP_EQUALSAS400     => 134,

    SCANCODE_INTERNATIONAL1     => 135, # used on Asian keyboards,
                                        # see footnotes in USB doc
    SCANCODE_INTERNATIONAL2     => 136,
    SCANCODE_INTERNATIONAL3     => 137, # Yen
    SCANCODE_INTERNATIONAL4     => 138,
    SCANCODE_INTERNATIONAL5     => 139,
    SCANCODE_INTERNATIONAL6     => 140,
    SCANCODE_INTERNATIONAL7     => 141,
    SCANCODE_INTERNATIONAL8     => 142,
    SCANCODE_INTERNATIONAL9     => 143,
    SCANCODE_LANG1              => 144, # Hangul/English toggle
    SCANCODE_LANG2              => 145, # Hanja conversion
    SCANCODE_LANG3              => 146, # Katakana
    SCANCODE_LANG4              => 147, # Hiragana
    SCANCODE_LANG5              => 148, # Zenkaku/Hankaku
    SCANCODE_LANG6              => 149, # reserved
    SCANCODE_LANG7              => 150, # reserved
    SCANCODE_LANG8              => 151, # reserved
    SCANCODE_LANG9              => 152, # reserved

    SCANCODE_ALTERASE           => 153, # Erase-Eaze
    SCANCODE_SYSREQ             => 154,
    SCANCODE_CANCEL             => 155,
    SCANCODE_CLEAR              => 156,
    SCANCODE_PRIOR              => 157,
    SCANCODE_RETURN2            => 158,
    SCANCODE_SEPARATOR          => 159,
    SCANCODE_OUT                => 160,
    SCANCODE_OPER               => 161,
    SCANCODE_CLEARAGAIN         => 162,
    SCANCODE_CRSEL              => 163,
    SCANCODE_EXSEL              => 164,

    SCANCODE_KP_00              => 176,
    SCANCODE_KP_000             => 177,
    SCANCODE_THOUSANDSSEPARATOR => 178,
    SCANCODE_DECIMALSEPARATOR   => 179,
    SCANCODE_CURRENCYUNIT       => 180,
    SCANCODE_CURRENCYSUBUNIT    => 181,
    SCANCODE_KP_LEFTPAREN       => 182,
    SCANCODE_KP_RIGHTPAREN      => 183,
    SCANCODE_KP_LEFTBRACE       => 184,
    SCANCODE_KP_RIGHTBRACE      => 185,
    SCANCODE_KP_TAB             => 186,
    SCANCODE_KP_BACKSPACE       => 187,
    SCANCODE_KP_A               => 188,
    SCANCODE_KP_B               => 189,
    SCANCODE_KP_C               => 190,
    SCANCODE_KP_D               => 191,
    SCANCODE_KP_E               => 192,
    SCANCODE_KP_F               => 193,
    SCANCODE_KP_XOR             => 194,
    SCANCODE_KP_POWER           => 195,
    SCANCODE_KP_PERCENT         => 196,
    SCANCODE_KP_LESS            => 197,
    SCANCODE_KP_GREATER         => 198,
    SCANCODE_KP_AMPERSAND       => 199,
    SCANCODE_KP_DBLAMPERSAND    => 200,
    SCANCODE_KP_VERTICALBAR     => 201,
    SCANCODE_KP_DBLVERTICALBAR  => 202,
    SCANCODE_KP_COLON           => 203,
    SCANCODE_KP_HASH            => 204,
    SCANCODE_KP_SPACE           => 205,
    SCANCODE_KP_AT              => 206,
    SCANCODE_KP_EXCLAM          => 207,
    SCANCODE_KP_MEMSTORE        => 208,
    SCANCODE_KP_MEMRECALL       => 209,
    SCANCODE_KP_MEMCLEAR        => 210,
    SCANCODE_KP_MEMADD          => 211,
    SCANCODE_KP_MEMSUBTRACT     => 212,
    SCANCODE_KP_MEMMULTIPLY     => 213,
    SCANCODE_KP_MEMDIVIDE       => 214,
    SCANCODE_KP_PLUSMINUS       => 215,
    SCANCODE_KP_CLEAR           => 216,
    SCANCODE_KP_CLEARENTRY      => 217,
    SCANCODE_KP_BINARY          => 218,
    SCANCODE_KP_OCTAL           => 219,
    SCANCODE_KP_DECIMAL         => 220,
    SCANCODE_KP_HEXADECIMAL     => 221,

    SCANCODE_LCTRL              => 224,
    SCANCODE_LSHIFT             => 225,
    SCANCODE_LALT               => 226, # alt, option
    SCANCODE_LGUI               => 227, # windows, command (apple), meta
    SCANCODE_RCTRL              => 228,
    SCANCODE_RSHIFT             => 229,
    SCANCODE_RALT               => 230, # alt gr, option
    SCANCODE_RGUI               => 231, # windows, command (apple), meta

    SCANCODE_MODE               => 257,

    # Usage page 0x0C
    SCANCODE_AUDIONEXT          => 258,
    SCANCODE_AUDIOPREV          => 259,
    SCANCODE_AUDIOSTOP          => 260,
    SCANCODE_AUDIOPLAY          => 261,
    SCANCODE_AUDIOMUTE          => 262,
    SCANCODE_MEDIASELECT        => 263,
    SCANCODE_WWW                => 264,
    SCANCODE_MAIL               => 265,
    SCANCODE_CALCULATOR         => 266,
    SCANCODE_COMPUTER           => 267,
    SCANCODE_AC_SEARCH          => 268,
    SCANCODE_AC_HOME            => 269,
    SCANCODE_AC_BACK            => 270,
    SCANCODE_AC_FORWARD         => 271,
    SCANCODE_AC_STOP            => 272,
    SCANCODE_AC_REFRESH         => 273,
    SCANCODE_AC_BOOKMARKS       => 274,

    # Walther keys
    SCANCODE_BRIGHTNESSDOWN     => 275,
    SCANCODE_BRIGHTNESSUP       => 276,
    SCANCODE_DISPLAYSWITCH      => 277, # display mirroring/dual display
                                        # switch, video mode switch
    SCANCODE_KBDILLUMTOGGLE     => 278,
    SCANCODE_KBDILLUMDOWN       => 279,
    SCANCODE_KBDILLUMUP         => 280,
    SCANCODE_EJECT              => 281,
    SCANCODE_SLEEP              => 282,

    SCANCODE_APP1               => 283,
    SCANCODE_APP2               => 284,

    # Usage page 0x0C (additional media keys)
    SCANCODE_AUDIOREWIND        => 285,
    SCANCODE_AUDIOFASTFORWARD   => 286,

    NUM_SCANCODES               => 512 # not a key: number of scancodes
);

our constant SCANCODE_MASK = 1 +< 30;
our sub SCANCODE_TO_KEYCODE ( $x ) { $x +| SCANCODE_MASK }

our enum Keycode (
    K_UNKNOWN            => 0,

    K_RETURN             => "\r".ord,
    K_ESCAPE             => 0o33,
    K_BACKSPACE          => "\b".ord,
    K_TAB                => "\t".ord,
    K_SPACE              => ' '.ord,
    K_EXCLAIM            => '!'.ord,
    K_QUOTEDBL           => '"'.ord,
    K_HASH               => '#'.ord,
    K_PERCENT            => '%'.ord,
    K_DOLLAR             => '$'.ord,
    K_AMPERSAND          => '&'.ord,
    K_QUOTE              => "'".ord,
    K_LEFTPAREN          => '('.ord,
    K_RIGHTPAREN         => ')'.ord,
    K_ASTERISK           => '*'.ord,
    K_PLUS               => '+'.ord,
    K_COMMA              => ','.ord,
    K_MINUS              => '-'.ord,
    K_PERIOD             => '.'.ord,
    K_SLASH              => '/'.ord,
    K_0                  => '0'.ord,
    K_1                  => '1'.ord,
    K_2                  => '2'.ord,
    K_3                  => '3'.ord,
    K_4                  => '4'.ord,
    K_5                  => '5'.ord,
    K_6                  => '6'.ord,
    K_7                  => '7'.ord,
    K_8                  => '8'.ord,
    K_9                  => '9'.ord,
    K_COLON              => ':'.ord,
    K_SEMICOLON          => ';'.ord,
    K_LESS               => '<'.ord,
    K_EQUALS             => '='.ord,
    K_GREATER            => '>'.ord,
    K_QUESTION           => '?'.ord,
    K_AT                 => '@'.ord,

    # Skip uppercase letters

    K_LEFTBRACKET        => '['.ord,
    K_BACKSLASH          => Q[\].ord,
    K_RIGHTBRACKET       => ']'.ord,
    K_CARET              => '^'.ord,
    K_UNDERSCORE         => '_'.ord,
    K_BACKQUOTE          => '`'.ord,
    K_a                  => 'a'.ord,
    K_b                  => 'b'.ord,
    K_c                  => 'c'.ord,
    K_d                  => 'd'.ord,
    K_e                  => 'e'.ord,
    K_f                  => 'f'.ord,
    K_g                  => 'g'.ord,
    K_h                  => 'h'.ord,
    K_i                  => 'i'.ord,
    K_j                  => 'j'.ord,
    K_k                  => 'k'.ord,
    K_l                  => 'l'.ord,
    K_m                  => 'm'.ord,
    K_n                  => 'n'.ord,
    K_o                  => 'o'.ord,
    K_p                  => 'p'.ord,
    K_q                  => 'q'.ord,
    K_r                  => 'r'.ord,
    K_s                  => 's'.ord,
    K_t                  => 't'.ord,
    K_u                  => 'u'.ord,
    K_v                  => 'v'.ord,
    K_w                  => 'w'.ord,
    K_x                  => 'x'.ord,
    K_y                  => 'y'.ord,
    K_z                  => 'z'.ord,

    K_CAPSLOCK           => SCANCODE_TO_KEYCODE(SCANCODE_CAPSLOCK),

    K_F1                 => SCANCODE_TO_KEYCODE(SCANCODE_F1),
    K_F2                 => SCANCODE_TO_KEYCODE(SCANCODE_F2),
    K_F3                 => SCANCODE_TO_KEYCODE(SCANCODE_F3),
    K_F4                 => SCANCODE_TO_KEYCODE(SCANCODE_F4),
    K_F5                 => SCANCODE_TO_KEYCODE(SCANCODE_F5),
    K_F6                 => SCANCODE_TO_KEYCODE(SCANCODE_F6),
    K_F7                 => SCANCODE_TO_KEYCODE(SCANCODE_F7),
    K_F8                 => SCANCODE_TO_KEYCODE(SCANCODE_F8),
    K_F9                 => SCANCODE_TO_KEYCODE(SCANCODE_F9),
    K_F10                => SCANCODE_TO_KEYCODE(SCANCODE_F10),
    K_F11                => SCANCODE_TO_KEYCODE(SCANCODE_F11),
    K_F12                => SCANCODE_TO_KEYCODE(SCANCODE_F12),

    K_PRINTSCREEN        => SCANCODE_TO_KEYCODE(SCANCODE_PRINTSCREEN),
    K_SCROLLLOCK         => SCANCODE_TO_KEYCODE(SCANCODE_SCROLLLOCK),
    K_PAUSE              => SCANCODE_TO_KEYCODE(SCANCODE_PAUSE),
    K_INSERT             => SCANCODE_TO_KEYCODE(SCANCODE_INSERT),
    K_HOME               => SCANCODE_TO_KEYCODE(SCANCODE_HOME),
    K_PAGEUP             => SCANCODE_TO_KEYCODE(SCANCODE_PAGEUP),
    K_DELETE             => 0o177,
    K_END                => SCANCODE_TO_KEYCODE(SCANCODE_END),
    K_PAGEDOWN           => SCANCODE_TO_KEYCODE(SCANCODE_PAGEDOWN),
    K_RIGHT              => SCANCODE_TO_KEYCODE(SCANCODE_RIGHT),
    K_LEFT               => SCANCODE_TO_KEYCODE(SCANCODE_LEFT),
    K_DOWN               => SCANCODE_TO_KEYCODE(SCANCODE_DOWN),
    K_UP                 => SCANCODE_TO_KEYCODE(SCANCODE_UP),

    K_NUMLOCKCLEAR       => SCANCODE_TO_KEYCODE(SCANCODE_NUMLOCKCLEAR),
    K_KP_DIVIDE          => SCANCODE_TO_KEYCODE(SCANCODE_KP_DIVIDE),
    K_KP_MULTIPLY        => SCANCODE_TO_KEYCODE(SCANCODE_KP_MULTIPLY),
    K_KP_MINUS           => SCANCODE_TO_KEYCODE(SCANCODE_KP_MINUS),
    K_KP_PLUS            => SCANCODE_TO_KEYCODE(SCANCODE_KP_PLUS),
    K_KP_ENTER           => SCANCODE_TO_KEYCODE(SCANCODE_KP_ENTER),
    K_KP_1               => SCANCODE_TO_KEYCODE(SCANCODE_KP_1),
    K_KP_2               => SCANCODE_TO_KEYCODE(SCANCODE_KP_2),
    K_KP_3               => SCANCODE_TO_KEYCODE(SCANCODE_KP_3),
    K_KP_4               => SCANCODE_TO_KEYCODE(SCANCODE_KP_4),
    K_KP_5               => SCANCODE_TO_KEYCODE(SCANCODE_KP_5),
    K_KP_6               => SCANCODE_TO_KEYCODE(SCANCODE_KP_6),
    K_KP_7               => SCANCODE_TO_KEYCODE(SCANCODE_KP_7),
    K_KP_8               => SCANCODE_TO_KEYCODE(SCANCODE_KP_8),
    K_KP_9               => SCANCODE_TO_KEYCODE(SCANCODE_KP_9),
    K_KP_0               => SCANCODE_TO_KEYCODE(SCANCODE_KP_0),
    K_KP_PERIOD          => SCANCODE_TO_KEYCODE(SCANCODE_KP_PERIOD),

    K_APPLICATION        => SCANCODE_TO_KEYCODE(SCANCODE_APPLICATION),
    K_POWER              => SCANCODE_TO_KEYCODE(SCANCODE_POWER),
    K_KP_EQUALS          => SCANCODE_TO_KEYCODE(SCANCODE_KP_EQUALS),
    K_F13                => SCANCODE_TO_KEYCODE(SCANCODE_F13),
    K_F14                => SCANCODE_TO_KEYCODE(SCANCODE_F14),
    K_F15                => SCANCODE_TO_KEYCODE(SCANCODE_F15),
    K_F16                => SCANCODE_TO_KEYCODE(SCANCODE_F16),
    K_F17                => SCANCODE_TO_KEYCODE(SCANCODE_F17),
    K_F18                => SCANCODE_TO_KEYCODE(SCANCODE_F18),
    K_F19                => SCANCODE_TO_KEYCODE(SCANCODE_F19),
    K_F20                => SCANCODE_TO_KEYCODE(SCANCODE_F20),
    K_F21                => SCANCODE_TO_KEYCODE(SCANCODE_F21),
    K_F22                => SCANCODE_TO_KEYCODE(SCANCODE_F22),
    K_F23                => SCANCODE_TO_KEYCODE(SCANCODE_F23),
    K_F24                => SCANCODE_TO_KEYCODE(SCANCODE_F24),
    K_EXECUTE            => SCANCODE_TO_KEYCODE(SCANCODE_EXECUTE),
    K_HELP               => SCANCODE_TO_KEYCODE(SCANCODE_HELP),
    K_MENU               => SCANCODE_TO_KEYCODE(SCANCODE_MENU),
    K_SELECT             => SCANCODE_TO_KEYCODE(SCANCODE_SELECT),
    K_STOP               => SCANCODE_TO_KEYCODE(SCANCODE_STOP),
    K_AGAIN              => SCANCODE_TO_KEYCODE(SCANCODE_AGAIN),
    K_UNDO               => SCANCODE_TO_KEYCODE(SCANCODE_UNDO),
    K_CUT                => SCANCODE_TO_KEYCODE(SCANCODE_CUT),
    K_COPY               => SCANCODE_TO_KEYCODE(SCANCODE_COPY),
    K_PASTE              => SCANCODE_TO_KEYCODE(SCANCODE_PASTE),
    K_FIND               => SCANCODE_TO_KEYCODE(SCANCODE_FIND),
    K_MUTE               => SCANCODE_TO_KEYCODE(SCANCODE_MUTE),
    K_VOLUMEUP           => SCANCODE_TO_KEYCODE(SCANCODE_VOLUMEUP),
    K_VOLUMEDOWN         => SCANCODE_TO_KEYCODE(SCANCODE_VOLUMEDOWN),
    K_KP_COMMA           => SCANCODE_TO_KEYCODE(SCANCODE_KP_COMMA),
    K_KP_EQUALSAS400     => SCANCODE_TO_KEYCODE(SCANCODE_KP_EQUALSAS400),

    K_ALTERASE           => SCANCODE_TO_KEYCODE(SCANCODE_ALTERASE),
    K_SYSREQ             => SCANCODE_TO_KEYCODE(SCANCODE_SYSREQ),
    K_CANCEL             => SCANCODE_TO_KEYCODE(SCANCODE_CANCEL),
    K_CLEAR              => SCANCODE_TO_KEYCODE(SCANCODE_CLEAR),
    K_PRIOR              => SCANCODE_TO_KEYCODE(SCANCODE_PRIOR),
    K_RETURN2            => SCANCODE_TO_KEYCODE(SCANCODE_RETURN2),
    K_SEPARATOR          => SCANCODE_TO_KEYCODE(SCANCODE_SEPARATOR),
    K_OUT                => SCANCODE_TO_KEYCODE(SCANCODE_OUT),
    K_OPER               => SCANCODE_TO_KEYCODE(SCANCODE_OPER),
    K_CLEARAGAIN         => SCANCODE_TO_KEYCODE(SCANCODE_CLEARAGAIN),
    K_CRSEL              => SCANCODE_TO_KEYCODE(SCANCODE_CRSEL),
    K_EXSEL              => SCANCODE_TO_KEYCODE(SCANCODE_EXSEL),

    K_KP_00              => SCANCODE_TO_KEYCODE(SCANCODE_KP_00),
    K_KP_000             => SCANCODE_TO_KEYCODE(SCANCODE_KP_000),
    K_THOUSANDSSEPARATOR => SCANCODE_TO_KEYCODE(SCANCODE_THOUSANDSSEPARATOR),
    K_DECIMALSEPARATOR   => SCANCODE_TO_KEYCODE(SCANCODE_DECIMALSEPARATOR),
    K_CURRENCYUNIT       => SCANCODE_TO_KEYCODE(SCANCODE_CURRENCYUNIT),
    K_CURRENCYSUBUNIT    => SCANCODE_TO_KEYCODE(SCANCODE_CURRENCYSUBUNIT),
    K_KP_LEFTPAREN       => SCANCODE_TO_KEYCODE(SCANCODE_KP_LEFTPAREN),
    K_KP_RIGHTPAREN      => SCANCODE_TO_KEYCODE(SCANCODE_KP_RIGHTPAREN),
    K_KP_LEFTBRACE       => SCANCODE_TO_KEYCODE(SCANCODE_KP_LEFTBRACE),
    K_KP_RIGHTBRACE      => SCANCODE_TO_KEYCODE(SCANCODE_KP_RIGHTBRACE),
    K_KP_TAB             => SCANCODE_TO_KEYCODE(SCANCODE_KP_TAB),
    K_KP_BACKSPACE       => SCANCODE_TO_KEYCODE(SCANCODE_KP_BACKSPACE),
    K_KP_A               => SCANCODE_TO_KEYCODE(SCANCODE_KP_A),
    K_KP_B               => SCANCODE_TO_KEYCODE(SCANCODE_KP_B),
    K_KP_C               => SCANCODE_TO_KEYCODE(SCANCODE_KP_C),
    K_KP_D               => SCANCODE_TO_KEYCODE(SCANCODE_KP_D),
    K_KP_E               => SCANCODE_TO_KEYCODE(SCANCODE_KP_E),
    K_KP_F               => SCANCODE_TO_KEYCODE(SCANCODE_KP_F),
    K_KP_XOR             => SCANCODE_TO_KEYCODE(SCANCODE_KP_XOR),
    K_KP_POWER           => SCANCODE_TO_KEYCODE(SCANCODE_KP_POWER),
    K_KP_PERCENT         => SCANCODE_TO_KEYCODE(SCANCODE_KP_PERCENT),
    K_KP_LESS            => SCANCODE_TO_KEYCODE(SCANCODE_KP_LESS),
    K_KP_GREATER         => SCANCODE_TO_KEYCODE(SCANCODE_KP_GREATER),
    K_KP_AMPERSAND       => SCANCODE_TO_KEYCODE(SCANCODE_KP_AMPERSAND),
    K_KP_DBLAMPERSAND    => SCANCODE_TO_KEYCODE(SCANCODE_KP_DBLAMPERSAND),
    K_KP_VERTICALBAR     => SCANCODE_TO_KEYCODE(SCANCODE_KP_VERTICALBAR),
    K_KP_DBLVERTICALBAR  => SCANCODE_TO_KEYCODE(SCANCODE_KP_DBLVERTICALBAR),
    K_KP_COLON           => SCANCODE_TO_KEYCODE(SCANCODE_KP_COLON),
    K_KP_HASH            => SCANCODE_TO_KEYCODE(SCANCODE_KP_HASH),
    K_KP_SPACE           => SCANCODE_TO_KEYCODE(SCANCODE_KP_SPACE),
    K_KP_AT              => SCANCODE_TO_KEYCODE(SCANCODE_KP_AT),
    K_KP_EXCLAM          => SCANCODE_TO_KEYCODE(SCANCODE_KP_EXCLAM),
    K_KP_MEMSTORE        => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMSTORE),
    K_KP_MEMRECALL       => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMRECALL),
    K_KP_MEMCLEAR        => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMCLEAR),
    K_KP_MEMADD          => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMADD),
    K_KP_MEMSUBTRACT     => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMSUBTRACT),
    K_KP_MEMMULTIPLY     => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMMULTIPLY),
    K_KP_MEMDIVIDE       => SCANCODE_TO_KEYCODE(SCANCODE_KP_MEMDIVIDE),
    K_KP_PLUSMINUS       => SCANCODE_TO_KEYCODE(SCANCODE_KP_PLUSMINUS),
    K_KP_CLEAR           => SCANCODE_TO_KEYCODE(SCANCODE_KP_CLEAR),
    K_KP_CLEARENTRY      => SCANCODE_TO_KEYCODE(SCANCODE_KP_CLEARENTRY),
    K_KP_BINARY          => SCANCODE_TO_KEYCODE(SCANCODE_KP_BINARY),
    K_KP_OCTAL           => SCANCODE_TO_KEYCODE(SCANCODE_KP_OCTAL),
    K_KP_DECIMAL         => SCANCODE_TO_KEYCODE(SCANCODE_KP_DECIMAL),
    K_KP_HEXADECIMAL     => SCANCODE_TO_KEYCODE(SCANCODE_KP_HEXADECIMAL),

    K_LCTRL              => SCANCODE_TO_KEYCODE(SCANCODE_LCTRL),
    K_LSHIFT             => SCANCODE_TO_KEYCODE(SCANCODE_LSHIFT),
    K_LALT               => SCANCODE_TO_KEYCODE(SCANCODE_LALT),
    K_LGUI               => SCANCODE_TO_KEYCODE(SCANCODE_LGUI),
    K_RCTRL              => SCANCODE_TO_KEYCODE(SCANCODE_RCTRL),
    K_RSHIFT             => SCANCODE_TO_KEYCODE(SCANCODE_RSHIFT),
    K_RALT               => SCANCODE_TO_KEYCODE(SCANCODE_RALT),
    K_RGUI               => SCANCODE_TO_KEYCODE(SCANCODE_RGUI),

    K_MODE               => SCANCODE_TO_KEYCODE(SCANCODE_MODE),

    K_AUDIONEXT          => SCANCODE_TO_KEYCODE(SCANCODE_AUDIONEXT),
    K_AUDIOPREV          => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOPREV),
    K_AUDIOSTOP          => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOSTOP),
    K_AUDIOPLAY          => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOPLAY),
    K_AUDIOMUTE          => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOMUTE),
    K_MEDIASELECT        => SCANCODE_TO_KEYCODE(SCANCODE_MEDIASELECT),
    K_WWW                => SCANCODE_TO_KEYCODE(SCANCODE_WWW),
    K_MAIL               => SCANCODE_TO_KEYCODE(SCANCODE_MAIL),
    K_CALCULATOR         => SCANCODE_TO_KEYCODE(SCANCODE_CALCULATOR),
    K_COMPUTER           => SCANCODE_TO_KEYCODE(SCANCODE_COMPUTER),
    K_AC_SEARCH          => SCANCODE_TO_KEYCODE(SCANCODE_AC_SEARCH),
    K_AC_HOME            => SCANCODE_TO_KEYCODE(SCANCODE_AC_HOME),
    K_AC_BACK            => SCANCODE_TO_KEYCODE(SCANCODE_AC_BACK),
    K_AC_FORWARD         => SCANCODE_TO_KEYCODE(SCANCODE_AC_FORWARD),
    K_AC_STOP            => SCANCODE_TO_KEYCODE(SCANCODE_AC_STOP),
    K_AC_REFRESH         => SCANCODE_TO_KEYCODE(SCANCODE_AC_REFRESH),
    K_AC_BOOKMARKS       => SCANCODE_TO_KEYCODE(SCANCODE_AC_BOOKMARKS),

    K_BRIGHTNESSDOWN     => SCANCODE_TO_KEYCODE(SCANCODE_BRIGHTNESSDOWN),
    K_BRIGHTNESSUP       => SCANCODE_TO_KEYCODE(SCANCODE_BRIGHTNESSUP),
    K_DISPLAYSWITCH      => SCANCODE_TO_KEYCODE(SCANCODE_DISPLAYSWITCH),
    K_KBDILLUMTOGGLE     => SCANCODE_TO_KEYCODE(SCANCODE_KBDILLUMTOGGLE),
    K_KBDILLUMDOWN       => SCANCODE_TO_KEYCODE(SCANCODE_KBDILLUMDOWN),
    K_KBDILLUMUP         => SCANCODE_TO_KEYCODE(SCANCODE_KBDILLUMUP),
    K_EJECT              => SCANCODE_TO_KEYCODE(SCANCODE_EJECT),
    K_SLEEP              => SCANCODE_TO_KEYCODE(SCANCODE_SLEEP),
    K_APP1               => SCANCODE_TO_KEYCODE(SCANCODE_APP1),
    K_APP2               => SCANCODE_TO_KEYCODE(SCANCODE_APP2),

    K_AUDIOREWIND        => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOREWIND),
    K_AUDIOFASTFORWARD   => SCANCODE_TO_KEYCODE(SCANCODE_AUDIOFASTFORWARD)
);

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L55
our enum EventType (
    FIRSTEVENT                 => 0,
    QUIT                       => 0x100, # User-requested quit

    # These application events are mostly for use on mobile platforms
    'APP_TERMINATING',                   # App was terminated by the OS
    'APP_LOWMEMORY',                     # App is low on memory
    'APP_WILLENTERBACKGROUND',           # App is about to enter the background
    'APP_DIDENTERBACKGROUND',            # App entered the background
    'APP_WILLENTERFOREGROUND',           # App is about to enter the foreground
    'APP_DIDENTERFOREGROUND',            # App is now interactive
    'LOCALECHANGED',                     # User's locale preferences have changed

    # Display events
     DISPLAYEVENT              => 0x150, # Display state change

    # Window events */
     WINDOWEVENT               => 0x200, # Window state change
    'SYSWMEVENT',                        # System specific event

    # Keyboard events
     KEYDOWN                   => 0x300, # Key pressed
    'KEYUP',                             # Key released
    'TEXTEDITING',                       # Keyboard text editing (composition)
    'TEXTINPUT',                         # Keyboard text input
    'KEYMAPCHANGED',                     # Keymap changed due to a system event

    # Mouse events
     MOUSEMOTION               => 0x400, # Mouse moved
    'MOUSEBUTTONDOWN',                   # Mouse button pressed
    'MOUSEBUTTONUP',                     # Mouse button released
    'MOUSEWHEEL',                        # Mouse wheel motion

    # Joystick events
     JOYAXISMOTION             => 0x600, # Joystick axis motion
    'JOYBALLMOTION',                     # Joystick trackball motion
    'JOYHATMOTION',                      # Joystick hat position change
    'JOYBUTTONDOWN',                     # Joystick button pressed
    'JOYBUTTONUP',                       # Joystick button released
    'JOYDEVICEADDED',                    # A new joystick was inserted into the system
    'JOYDEVICEREMOVED',                  # An opened joystick has been removed

    # Game controller events
     CONTROLLERAXISMOTION      => 0x650, # Game controller axis motion
    'CONTROLLERBUTTONDOWN',              # Game controller button pressed
    'CONTROLLERBUTTONUP',                # Game controller button released
    'CONTROLLERDEVICEADDED',             # A new controller was inserted
    'CONTROLLERDEVICEREMOVED',           # An open controller has been removed
    'CONTROLLERDEVICEREMAPPED',          # The controller mapping was updated
    'CONTROLLERTOUCHPADDOWN',            # Touchpad was touched
    'CONTROLLERTOUCHPADMOTION',          # Touchpad finger was moved
    'CONTROLLERTOUCHPADUP',              # Touchpad finger was lifted
    'CONTROLLERSENSORUPDATE',            # Sensor was updated

    # Touch events
     FINGERDOWN                => 0x700,
    'FINGERUP',
    'FINGERMOTION',

    # Gesture events
     DOLLARGESTURE             => 0x800,
    'DOLLARRECORD',
    'MULTIGESTURE',

    # Clipboard events
     CLIPBOARDUPDATE           => 0x900,  # The clipboard changed

    # Drag and drop events
     DROPFILE                  => 0x1000, # The system requests a file open
    'DROPTEXT',                           # text/plain drag-and-drop event
    'DROPBEGIN',                          # A new set of drops is beginning
    'DROPCOMPLETE',                       # Current set of drops is now complete

    #  Audio hotplug events
     AUDIODEVICEADDED          => 0x1100, # A new audio device is available
    'AUDIODEVICEREMOVED',                 # An audio device has been removed

    # Sensor events
     SENSORUPDATE              => 0x1200, # A sensor was updated

    # Render events
     RENDER_TARGETS_RESET      => 0x2000, # The render targets have been reset
                                          # and their contents need to be updated
    'RENDER_DEVICE_RESET',                # The device has been reset and all
                                          # textures need to be recreated

    # Events USEREVENT through LASTEVENT are for your use,
    # and should be allocated with SDL::RegisterEvents()
    USEREVENT                  => 0x8000,
    LASTEVENT                  => 0xFFFF,
);

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_video.h#L146
our enum WindowEventType (
    WINDOWEVENT_NONE         =>  0, # Never used
    WINDOWEVENT_SHOWN        =>  1, # Window has been shown
    WINDOWEVENT_HIDDEN       =>  2, # Window has been hidden
    WINDOWEVENT_EXPOSED      =>  3, # Window has been exposed and should
                                    # be redrawn
    WINDOWEVENT_MOVED        =>  4, # Window has been moved
                                    # to data1, data2
    WINDOWEVENT_RESIZED      =>  5, # Window has been resized
                                    # to data1xdata2
    WINDOWEVENT_SIZE_CHANGED =>  6, # The window size has changed, either
                                    # as a result of an API call or
                                    # through the system or user changing
                                    # the window size
    WINDOWEVENT_MINIMIZED    =>  7, # Window has been minimized
    WINDOWEVENT_MAXIMIZED    =>  8, # Window has been maximized
    WINDOWEVENT_RESTORED     =>  9, # Window has been restored to normal
                                    # size and position
    WINDOWEVENT_ENTER        => 10, # Window has gained mouse focus
    WINDOWEVENT_LEAVE        => 11, # Window has lost mouse focus
    WINDOWEVENT_FOCUS_GAINED => 12, # Window has gained keyboard focus
    WINDOWEVENT_FOCUS_LOST   => 13, # Window has lost keyboard focus
    WINDOWEVENT_CLOSE        => 14, # The window manager requests that
                                    # the window be closed
    WINDOWEVENT_TAKE_FOCUS   => 15, # Window is being offered a focus
                                    # (should SetWindowInputFocus() on
                                    # itself or a subwindow, or ignore)
    WINDOWEVENT_HIT_TEST     => 16, # Window had a hit test that wasn't
                                    # SDL_HITTEST_NORMAL.
);

our enum MouseButton (
     BUTTON_LEFT => 1,
    'BUTTON_MIDDLE',
    'BUTTON_RIGHT',
    'BUTTON_X1',
    'BUTTON_X2',
);

our enum TextureAccess (
    TEXTUREACCESS_STATIC    => 0b00,
    TEXTUREACCESS_STREAMING => 0b01,
    TEXTUREACCESS_TARGET    => 0b10,
);

our enum RendererFlip (
    FLIP_NONE       => 0b00,
    FLIP_HORIZONTAL => 0b01,
    FLIP_VERTICAL   => 0b10,
);

our enum BlendMode (
    BLENDMODE_NONE    => 0b00000000, # no blending
                                     # dstRGBA = srcRGBA
    BLENDMODE_BLEND   => 0b00000001, # alpha blending
                                     # dstRGB = (srcRGB * srcA) + (dstRGB * (1-srcA))
                                     # dstA = srcA + (dstA * (1-srcA)) */
    BLENDMODE_ADD     => 0b00000010, # additive blending
                                     # dstRGB = (srcRGB * srcA) + dstRGB
                                     # dstA = dstA
    BLENDMODE_MOD     => 0b00000100, # color modulate
                                     # dstRGB = srcRGB * dstRGB
                                     # dstA = dstA
    BLENDMODE_MUL     => 0b00001000, # color multiply
                                     # dstRGB = (srcRGB * dstRGB) + (dstRGB * (1-srcA))
                                     # dstA = (srcA * dstA) + (dstA * (1-srcA))
    BLENDMODE_INVALID => 0x7FFFFFFF
);

our enum GameControllerType (
     CONTROLLER_TYPE_UNKNOWN => 0,
    'CONTROLLER_TYPE_XBOX360',
    'CONTROLLER_TYPE_XBOXONE',
    'CONTROLLER_TYPE_PS3',
    'CONTROLLER_TYPE_PS4',
    'CONTROLLER_TYPE_NINTENDO_SWITCH_PRO',
    'CONTROLLER_TYPE_VIRTUAL',
    'CONTROLLER_TYPE_PS5',
);

our enum GameControllerBindType (
     CONTROLLER_BINDTYPE_NONE => 0,
    'CONTROLLER_BINDTYPE_BUTTON',
    'CONTROLLER_BINDTYPE_AXIS',
    'CONTROLLER_BINDTYPE_HAT',
);

our enum GameControllerAxis (
     CONTROLLER_AXIS_INVALID => -1,
    'CONTROLLER_AXIS_LEFTX',
    'CONTROLLER_AXIS_LEFTY',
    'CONTROLLER_AXIS_RIGHTX',
    'CONTROLLER_AXIS_RIGHTY',
    'CONTROLLER_AXIS_TRIGGERLEFT',
    'CONTROLLER_AXIS_TRIGGERRIGHT',
    'CONTROLLER_AXIS_MAX',
);

our enum GameControllerButton (
     CONTROLLER_BUTTON_INVALID => -1,
    'CONTROLLER_BUTTON_A',
    'CONTROLLER_BUTTON_B',
    'CONTROLLER_BUTTON_X',
    'CONTROLLER_BUTTON_Y',
    'CONTROLLER_BUTTON_BACK',
    'CONTROLLER_BUTTON_GUIDE',
    'CONTROLLER_BUTTON_START',
    'CONTROLLER_BUTTON_LEFTSTICK',
    'CONTROLLER_BUTTON_RIGHTSTICK',
    'CONTROLLER_BUTTON_LEFTSHOULDER',
    'CONTROLLER_BUTTON_RIGHTSHOULDER',
    'CONTROLLER_BUTTON_DPAD_UP',
    'CONTROLLER_BUTTON_DPAD_DOWN',
    'CONTROLLER_BUTTON_DPAD_LEFT',
    'CONTROLLER_BUTTON_DPAD_RIGHT',
    'CONTROLLER_BUTTON_MISC1',    # Xbox Series X share button
                                  # PS5 microphone button
                                  # Nintendo Switch Pro capture button
    'CONTROLLER_BUTTON_PADDLE1',  # Xbox Elite paddle P1
    'CONTROLLER_BUTTON_PADDLE2',  # Xbox Elite paddle P3
    'CONTROLLER_BUTTON_PADDLE3',  # Xbox Elite paddle P2
    'CONTROLLER_BUTTON_PADDLE4',  # Xbox Elite paddle P4
    'CONTROLLER_BUTTON_TOUCHPAD', # PS4/PS5 touchpad button
    'CONTROLLER_BUTTON_MAX',
);

our enum Pixeltype (
    PIXELTYPE_UNKNOWN  =>  0,
    PIXELTYPE_INDEX1   =>  1,
    PIXELTYPE_INDEX4   =>  2,
    PIXELTYPE_INDEX8   =>  3,
    PIXELTYPE_PACKED8  =>  4,
    PIXELTYPE_PACKED16 =>  5,
    PIXELTYPE_PACKED32 =>  6,
    PIXELTYPE_ARRAYU8  =>  7,
    PIXELTYPE_ARRAYU16 =>  8,
    PIXELTYPE_ARRAYU32 =>  9,
    PIXELTYPE_ARRAYF16 => 10,
    PIXELTYPE_ARRAYF32 => 11,
);

our enum WindowFlags (
    WINDOW_FULLSCREEN         => 0x00000001, # fullscreen window
    WINDOW_OPENGL             => 0x00000002, # window usable with OpenGL context
    WINDOW_SHOWN              => 0x00000004, # window is visible
    WINDOW_HIDDEN             => 0x00000008, # window is not visible
    WINDOW_BORDERLESS         => 0x00000010, # no window decoration
    WINDOW_RESIZABLE          => 0x00000020, # window can be resized
    WINDOW_MINIMIZED          => 0x00000040, # window is minimized
    WINDOW_MAXIMIZED          => 0x00000080, # window is maximized
    WINDOW_MOUSE_GRABBED      => 0x00000100, # window has grabbed mouse input
    WINDOW_INPUT_GRABBED      => 0x00000100, # For compatibility
    WINDOW_INPUT_FOCUS        => 0x00000200, # window has input focus
    WINDOW_MOUSE_FOCUS        => 0x00000400, # window has mouse focus
    WINDOW_FOREIGN            => 0x00000800, # window not created by SDL
    WINDOW_FULLSCREEN_DESKTOP => 0x00001001,
    WINDOW_ALLOW_HIGHDPI      => 0x00002000, # window should be created in
                                             # high-DPI mode if supported
                                             # NSHighResolutionCapable must
                                             # be set true on macOS in the
                                             # application's Info.plist for
                                             # this to have any effect
    WINDOW_MOUSE_CAPTURE      => 0x00004000, # window has mouse captured
                                             # (unrelated to MOUSE_GRABBED)
    WINDOW_ALWAYS_ON_TOP      => 0x00008000, # window should always be
                                             # above others
    WINDOW_SKIP_TASKBAR       => 0x00010000, # do not add to the taskbar
    WINDOW_UTILITY            => 0x00020000, # treat as utility window
    WINDOW_TOOLTIP            => 0x00040000, # treat as a tooltip
    WINDOW_POPUP_MENU         => 0x00080000, # treat as a popup menu
    WINDOW_KEYBOARD_GRABBED   => 0x00100000, # window has grabbed keyboard input
    WINDOW_VULKAN             => 0x10000000, # window usable for Vulkan surface
    WINDOW_METAL              => 0x20000000, # window usable for Metal view
);

our class Renderer is repr('CPointer') { }

our class Window is repr('CPointer') { }

our class Joystick is repr('CPointer') { }

our class GameController is repr('CPointer') { }

our class Texture is repr('CPointer') { }

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L259
our class Surface is repr('CStruct') is rw {
    has uint32  $!flags;
    has Pointer $!format;
    has  int32  $.w;
    has  int32  $.h;
    has  int32  $.pitch;
}

our class Point is repr('CStruct') is rw {
    has int32 $.x;
    has int32 $.y;

    # Positional
    multi method new ( Int(Real) $x, Int(Real) $y ) { self.bless: :$x, :$y }
}

our class Rect is repr('CStruct') is rw {
    has int32 $.x;
    has int32 $.y;
    has int32 $.w;
    has int32 $.h;

    # Positional
    multi method new (
        int $x,
        int $y,
        int $w,
        int $h,
    ) { self.bless: :$x, :$y, :$w, :$h }

    multi method new (
        Int(Real) $x,
        Int(Real) $y,
        Int(Real) $w,
        Int(Real) $h,
    ) { self.bless: :$x, :$y, :$w, :$h }
}

our class KeyboardEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.windowID;
    has uint8  $.state;
    has uint8  $.repeat;
    has  int32 $.scancode;
    has  int32 $.sym;
    has uint16 $.mod;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L259
our class MouseMotionEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.windowID;
    has uint32 $.which;
    has uint32 $.state;
    has  int32 $.x;
    has  int32 $.y;
    has  int32 $.xrel;
    has  int32 $.yrel;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L275
our class MouseButtonEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.windowID;
    has uint32 $.which;
    has uint8  $.button;
    has uint8  $.state;
    has uint8  $.clicks;
    has uint8  $!padding;
    has  int32 $.x;
    has  int32 $.y;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L292
our class MouseWheelEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.windowID;
    has uint32 $.which;
    has  int32 $.x;
    has  int32 $.y;
    has  int32 $.direction;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L383
our class ControllerAxisEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has  int32 $.which; # SDL_JoystickID
    has uint8  $.axis;
    has uint8  $!padding1;
    has uint16 $!padding2;
    has  int16 $.value;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L400
our class ControllerButtonEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has  int32 $.which; # SDL_JoystickID
    has uint8  $.button;
    has uint8  $.state;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L415
our class ControllerDeviceEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has  int32 $.which; # SDL_JoystickID
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L425
our class ControllerTouchpadEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has  int32 $.which;    # SDL_JoystickID
    has  int32 $.touchpad;
    has  int32 $.finger;
    has  num32 $.x;        # float
    has  num32 $.y;        # float
    has  num32 $.pressure; # float
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L201
our class WindowEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.window-id;
    has uint8  $.event;
    has  int32 $.data1;
    has  int32 $.data2;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L520
our class DropEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has Str    $.file;
    has uint32 $.window-id;
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L248
our class TextInputEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.window-id;
    has uint8  $!ch01; # Hack to support char[32]
    has uint8  $!ch02; # See https://github.com/rakudo/rakudo/issues/3633
    has uint8  $!ch03;
    has uint8  $!ch04;
    has uint8  $!ch05;
    has uint8  $!ch06;
    has uint8  $!ch07;
    has uint8  $!ch08;
    has uint8  $!ch09;
    has uint8  $!ch10;
    has uint8  $!ch11;
    has uint8  $!ch12;
    has uint8  $!ch13;
    has uint8  $!ch14;
    has uint8  $!ch15;
    has uint8  $!ch16;
    has uint8  $!ch17;
    has uint8  $!ch18;
    has uint8  $!ch19;
    has uint8  $!ch20;
    has uint8  $!ch21;
    has uint8  $!ch22;
    has uint8  $!ch23;
    has uint8  $!ch24;
    has uint8  $!ch25;
    has uint8  $!ch26;
    has uint8  $!ch27;
    has uint8  $!ch28;
    has uint8  $!ch29;
    has uint8  $!ch30;
    has uint8  $!ch31;
    has uint8  $!ch32;

    method text ( --> Str ) {
        Buf.new(
            gather for self.^attributes.grep( *.name.starts-with: '$!ch' ).sort(*.name) {
                with .get_value(self) {
                    last when 0;
                    .take;
                }
            }
        ).decode
    }
}

# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L233
our class TextEditingEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has uint32 $.window-id;
    has uint8  $!ch01; # Hack to support char[32]
    has uint8  $!ch02; # See https://github.com/rakudo/rakudo/issues/3633
    has uint8  $!ch03;
    has uint8  $!ch04;
    has uint8  $!ch05;
    has uint8  $!ch06;
    has uint8  $!ch07;
    has uint8  $!ch08;
    has uint8  $!ch09;
    has uint8  $!ch10;
    has uint8  $!ch11;
    has uint8  $!ch12;
    has uint8  $!ch13;
    has uint8  $!ch14;
    has uint8  $!ch15;
    has uint8  $!ch16;
    has uint8  $!ch17;
    has uint8  $!ch18;
    has uint8  $!ch19;
    has uint8  $!ch20;
    has uint8  $!ch21;
    has uint8  $!ch22;
    has uint8  $!ch23;
    has uint8  $!ch24;
    has uint8  $!ch25;
    has uint8  $!ch26;
    has uint8  $!ch27;
    has uint8  $!ch28;
    has uint8  $!ch29;
    has uint8  $!ch30;
    has uint8  $!ch31;
    has uint8  $!ch32;
    has  int32 $.start;
    has  int32 $.length;

    method text ( --> Str ) {
        Buf.new(
            gather for self.^attributes.grep( *.name.starts-with: '$!ch' ).sort(*.name) {
                with .get_value(self) {
                    last when 0;
                    .take;
                }
            }
        ).decode
    }
}

our class JoyAxisEvent is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;
    has int32  $.which;
    has uint8  $.axis;
    has int32  $!value;

    method value { my int16 $foo = $!value }
}

# TODO: SDL_ControllerSensorEvent
# https://github.com/SDL-mirror/SDL/blob/master/include/SDL_events.h#L440

our class Event is repr('CStruct') is rw {
    has uint32 $.type;
    has uint32 $.timestamp;

    # Needs to pad until 56 bytes
    has  int64 $!dummy1;
    has  int64 $!dummy2;
    has  int64 $!dummy3;
    has  int64 $!dummy4;
    has  int64 $!dummy5;
    has  int64 $!dummy6;

    method csensor   { ... }
    method button    { nativecast( MouseButtonEvent,        self ) }
    method caxis     { nativecast( ControllerAxisEvent,     self ) }
    method cbutton   { nativecast( ControllerButtonEvent,   self ) }
    method cdevice   { nativecast( ControllerDeviceEvent,   self ) }
    method ctouchpad { nativecast( ControllerTouchpadEvent, self ) }
    method drop      { nativecast( DropEvent,               self ) }
    method edit      { nativecast( TextEditingEvent,        self ) }
    method jaxis     { nativecast( JoyAxisEvent,            self ) }
    method key       { nativecast( KeyboardEvent,           self ) }
    method motion    { nativecast( MouseMotionEvent,        self ) }
    method text      { nativecast( TextInputEvent,          self ) }
    method wheel     { nativecast( MouseWheelEvent,         self ) }
    method window    { nativecast( WindowEvent,             self ) }
}

our sub GetError
    returns Str
    is symbol('SDL_GetError')
    is native(LIB) {*}

our sub GetScancodeFromKey(int32 $key)
    returns int32
    is symbol('SDL_GetScancodeFromKey')
    is native(LIB) {*}

our sub EventState (int32 $type, int32 $state)
    returns uint8
    is symbol('SDL_EventState')
    is native(LIB) {*}

our sub PollEvent (Event $event)
    returns int32
    is symbol('SDL_PollEvent')
    is native(LIB) {*}

our sub PushEvent (Event $event)
    returns int32
    is symbol('SDL_PushEvent')
    is native(LIB) {*}

our sub RegisterEvents (int32 $numevents)
    returns uint32
    is symbol('SDL_RegisterEvents')
    is native(LIB) {*}

our sub Init (int32 $flags)
    returns int32
    is symbol('SDL_Init')
    is native(LIB) {*}

our sub WasInit (uint32 $flags)
    returns uint32
    is symbol('SDL_WasInit')
    is native(LIB) {*}

our sub InitSubSystem (uint32 $flags)
    returns uint32
    is symbol('SDL_InitSubSystem')
    is native(LIB) {*}

our sub Delay (uint32 $ms)
    is symbol('SDL_Delay')
    is native(LIB) {*}

our sub CreateRenderer ( Window $win, int32 $index, int32 $flags )
    returns Renderer
    is symbol('SDL_CreateRenderer')
    is native(LIB) {*}

our sub DestroyRenderer ( Renderer $renderer )
    is symbol('SDL_DestroyRenderer')
    is native(LIB) {*}

our sub CreateTexture ( Renderer $renderer, int32 $format, int32 $access, int32 $w, int32 $h )
    returns Texture
    is symbol('SDL_CreateTexture')
    is native(LIB) {*}

our sub DestroyTexture (Texture $texture)
    is symbol('SDL_DestroyTexture')
    is native(LIB) {*}

our sub RenderClear ( Renderer $renderer )
    returns int32
    is symbol('SDL_RenderClear')
    is native(LIB) {*}

our sub RenderPresent ( Renderer $renderer )
    returns int32
    is symbol('SDL_RenderPresent')
    is native(LIB) {*}

our sub RenderCopy ( Renderer $renderer, Texture $src, Rect $srcrect, Rect $destrect )
    returns int32
    is symbol('SDL_RenderCopy')
    is native(LIB) {*}

our sub RenderCopyEx (
    Renderer $renderer,
    Texture  $src,
    Rect     $srcrect,
    Rect     $destrect,
    num64    $angle,
    Point    $center,
    int32    $flip,
)
    returns int32
    is symbol('SDL_RenderCopyEx')
    is native(LIB) {*}

our sub RenderSetLogicalSize ( Renderer $renderer, int32 $w, int32 $h )
    returns int32
    is symbol('SDL_RenderSetLogicalSize')
    is native(LIB) {*}

our sub RenderGetLogicalSize ( Renderer $renderer, Pointer[int32] $w, Pointer[int32] $h )
    is symbol('SDL_RenderGetLogicalSize')
    is native(LIB) {*}

our sub SetRenderTarget ( Renderer $renderer, Texture $texture )
    returns int32
    is symbol('SDL_SetRenderTarget')
    is native(LIB) {*}

our sub SetRenderDrawColor ( Renderer $renderer, int8 $r, int8 $g, int8 $b, int8 $a )
    returns int32
    is symbol('SDL_SetRenderDrawColor')
    is native(LIB) {*}

our sub SetTextureBlendMode (Texture $tex, int32 $blendmode)
    returns int32
    is symbol('SDL_SetTextureBlendMode')
    is native(LIB) {*}

our sub GetTextureBlendMode (Texture $tex, Pointer[int32] $blendmode)
    returns int32
    is symbol('SDL_GetTextureBlendMode')
    is native(LIB) {*}

our sub SetTextureColorMod (Texture $tex, uint8 $r, uint8 $g, uint8 $b )
    returns int32
    is symbol('SDL_SetTextureColorMod')
    is native(LIB) {*}

our sub GetTextureColorMod (Texture $tex, uint8 $r is rw, uint8 $g is rw, uint8 $b is rw )
    returns int32
    is symbol('SDL_GetTextureColorMod')
    is native(LIB) {*}

our sub QueryTexture (
    Texture $texture,
    uint32 $format is rw,
    int32  $access is rw,
    int32  $w      is rw,
    int32  $h      is rw,
)
    returns int32
    is symbol('SDL_QueryTexture')
    is native(LIB) {*}

# Joystick exports

our sub NumJoysticks ()
    returns int32
    is symbol('SDL_NumJoysticks')
    is native(LIB) {*}

our sub JoystickOpen( int32 $idx )
    returns Joystick
    is symbol('SDL_JoystickOpen')
    is native(LIB) {*}

our sub JoystickClose( Joystick $joystick )
    is symbol('SDL_JoystickClose')
    is native(LIB) {*}

our sub JoystickOpened( int32 $idx )
    returns int32
    is symbol('SDL_JoystickOpened')
    is native(LIB) {*}

our sub JoystickEventState( int32 $toggle )
    returns int32
    is symbol('SDL_JoystickEventState')
    is native(LIB) {*}

our sub JoystickInstanceID( Joystick $joystick )
    returns int32
    is symbol('SDL_JoystickInstanceID')
    is native(LIB) {*}

our sub JoystickGetAttached ( Joystick $joystick )
    returns Bool
    is symbol('SDL_JoystickGetAttached')
    is native(LIB) {*}

# TODO: SDL_JoystickGetGUID and SDL_JoystickGetGUIDString

our sub GameControllerGetJoystick ( GameController $controller )
    returns Joystick
    is symbol('SDL_GameControllerGetJoystick')
    is native(LIB) {*}

our sub GameControllerOpen ( uint8 $index )
    returns GameController
    is symbol('SDL_GameControllerOpen')
    is native(LIB) {*}

our sub GameControllerClose ( GameController $controller )
    is symbol('SDL_GameControllerClose')
    is native(LIB) {*}

our sub IsGameController ( uint8 $index )
    returns Bool
    is symbol('SDL_IsGameController')
    is native(LIB) {*}

our sub GameControllerNameForIndex ( uint8 $index )
    returns str
    is symbol('SDL_GameControllerNameForIndex')
    is native(LIB) {*}

our sub CreateRGBSurface (
    uint32 $flags,
     int32 $width,
     int32 $height,
     int32 $depth,
    uint32 $rmask,
    uint32 $gmask,
    uint32 $bmask,
    uint32 $amask,
)
    returns Surface
    is symbol('SDL_CreateRGBSurface')
    is native(LIB) {*}

our sub SetSurfaceColorMod ( Surface $src, uint8 $r, uint8 $g, uint8 $b )
    returns int32
    is symbol('SDL_SetSurfaceColorMod')
    is native(LIB) {*}

our sub GetSurfaceColorMod ( Surface $src, uint8 $r is rw, uint8 $g is rw, uint8 $b is rw )
    returns int32
    is symbol('SDL_GetSurfaceColorMod')
    is native(LIB) {*}

our sub LockSurface ( Surface $src )
    returns int32
    is symbol('SDL_LockSurface')
    is native(LIB) {*}

our sub UnlockSurface ( Surface $src )
    is symbol('SDL_UnlockSurface')
    is native(LIB) {*}

our sub BlitSurface ( Surface $src, Rect $srcrect is rw, Surface $dst, Rect $dstrect is rw )
    returns int32
    is symbol('SDL_UpperBlit')
    is native(LIB) {*}

our sub CreateTextureFromSurface( Renderer $renderer, Surface $surface )
    returns Texture
    is symbol('SDL_CreateTextureFromSurface')
    is native(LIB) {*}

our sub FreeSurface ( Surface $surface )
    is symbol('SDL_FreeSurface')
    is native(LIB) {*}

# Text functions

our sub SetTextInputRect ( Rect $rect is rw )
    is symbol('SDL_SetTextInputRect')
    is native(LIB) {*}

our sub CreateWindow (
    Str $title,
    int32 $x,
    int32 $y,
    int32 $w,
    int32 $h,
    int32 $flags
)
    returns Window
    is symbol('SDL_CreateWindow')
    is native(LIB) {*}

our sub DestroyWindow ( Window $win )
    is symbol('SDL_DestroyWindow')
    is native(LIB) {*}

our sub ShowCursor ( int32 $toggle )
    returns int32
    is symbol('SDL_ShowCursor')
    is native(LIB) {*}

our sub GetWindowPixelFormat ( Window $window )
    returns uint32
    is symbol('SDL_GetWindowPixelFormat')
    is native(LIB) {*}
