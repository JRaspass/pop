unit package SDL::Image:ver<0.0.1>:auth<zef:jjatria>;

use NativeCall;
use Pop::SDL;

my constant LIB = 'SDL2_image';

our constant INIT_JPG  is export = 0x00000001; # JPG
our constant INIT_PNG  is export = 0x00000002; # PNG
our constant INIT_TIF  is export = 0x00000004; # TIF
our constant INIT_WEBP is export = 0x00000008; # WebP

our sub Init ( int32 $flag )
    returns int32
    is symbol('IMG_Init')
    is native(LIB) {*}

our sub Load ( Str $path )
    returns SDL::Surface
    is symbol('IMG_Load')
    is native(LIB) {*}

our sub LoadTexture ( SDL::Renderer $renderer, Str $path )
    returns SDL::Texture
    is symbol('IMG_LoadTexture')
    is native(LIB) {*}

our sub GetError ()
    returns str
    is symbol('SDL_GetError')
    is native('SDL2') {*}
