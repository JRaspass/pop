unit module Pop::Singleton:ver<0.0.2>:auth<zef:jjatria>;

role MetamodelX::SingletonHOW {
    my $bypass = set << new bless BUILDALL BUILD dispatch:<!> >>;

    method compose(Mu $obj) {
        callsame;
        for $obj.^method_table.kv -> $name, $code {
            next if $bypass{$name};

            # This is horrid but callwith needs to see the 'rw'
            # when the wrapper is compiled it seems
            $code.wrap: $code.rw
                ?? method (|c) is hidden-from-backtrace is rw {
                    callwith( self // self.new, |c ) }
                !! method (|c) is hidden-from-backtrace {
                    callwith( self // self.new, |c ) };
        }
    }
}

role Singleton {
    my $single;
    method new { $single //= callsame }
}

multi sub trait_mod:<is>(Mu:U $doee, :$Singleton!) is export {
    $doee.HOW does MetamodelX::SingletonHOW;
    $doee.^add_role(Singleton);
}
