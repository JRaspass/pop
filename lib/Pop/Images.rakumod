use Pop::Singleton;
unit class Pop::Images:ver<0.0.2>:auth<zef:jjatria> is Singleton;

use Pop::Point;
use Pop::Surface;
use Pop::SDL;
use Pop::SDL::Image;

has Pop::Surface %!surfaces;

submethod TWEAK {
    die "Could not initialise IMG: { SDL::Image::GetError }"
        unless SDL::Image::Init( SDL::Image::INIT_PNG +| SDL::Image::INIT_JPG );
}

# Loads an image file into memory.
method load (
    IO()  $path,
    Str() $key   = $path,
    Bool :$cache = True
    --> Pop::Surface
) {
    .return with %!surfaces{$key};

    note "Loading '{ $path.absolute }'" ~ ( " as '$key'..." if $cache );

    if SDL::Image::Load( ~$path ) -> $sdl {
        my $surface = Pop::Surface.new: surface => $sdl;
        return $cache ?? ( %!surfaces{ $key } = $surface ) !! $surface;
    }

    die 'Error loading image: ' ~ SDL::Image::GetError();
}

# Fetches a loaded image from cache by te specified key. If no image by that
# key has been loaded, a type object is returned. This function never reads
# from the filesystem.
method get ( Str:D() $key --> Pop::Surface ) { %!surfaces{$key} // Pop::Surface }

#| Unloads an image file from memory
multi method destroy ( Str:D() $key --> Nil ) {
    with %!surfaces{$key}:delete {
        note "Unloading $key...";
        SDL::FreeSurface($_)
    }
    else {
        die "No image with that key: $key";
    }
}

multi method destroy ( --> Nil ) {
    for %!surfaces.keys {
        note "Unloading $_...";
        SDL::FreeSurface( %!surfaces{$_}:delete );
    }
}
