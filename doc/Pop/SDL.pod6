=begin pod

=head1 NAME

Pop::SDL

=head1 SYNOPSIS

    use Pop::SDL;

=head1 DESCRIPTION

Pop::SDL includes a set of custom bindings to SDL2 for use by L<Pop>. It aims
to provide bindings to a reasonably complete set of the SDL2 functions,
constants, and enums that should be instantly recognisable to someone who is
familiar with SDL2 itself. This means that this library aims to provide little
if any syntactic sugar at all, and to keep names as close as possible to those
in the original headers.

Names are exported in the SDL Raku namespace replacing the C<SDL_> prefix in
the external symbols. When the prefix has any other characters added before
the leading underscore, these will be the first character in the local symbol.

This means that C<SDL_WINDOW_OPENGL> will become C<SDL::WINDOW_OPENGL>, and
C<SDLK_ESCAPE> will become C<SDL::K_ESCAPE>.

The information below serves as an indication of what this library implements.
For specifics, please refer to
L<the SDL2 documentation|https://wiki.libsdl.org/FrontPage>.

=head1 CLASSES

=head2 SDL::Point

=head3 new

    method new (
        Int(Real) $x,
        Int(Real) $y,
    ) returns SDL::Point

=head3 x

    method x () returns int32

=head3 y

    method y () returns int32

=head2 SDL::Rect

=head3 new

    multi method new (
        Int(Real) $x,
        Int(Real) $y,
        Int(Real) $w,
        Int(Real) $h,
    ) returns SDL::Rect

=head3 x

    method x () returns int32

=head3 y

    method y () returns int32

=head3 w

    method w () returns int32

=head3 h

    method h () returns int32

=head2 SDL::KeyboardEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 windowID

    method windowID () returns uint32

=head3 state

    method state () returns uint8

=head3 repeat

    method repeat () returns uint8

=head3 scancode

    method scancode () returns int32

=head3 sym

    method sym () returns int32

=head3 mod

    method mod () returns uint16

=head2 SDL::MouseMotionEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 windowID

    method windowID () returns uint32

=head3 which

    method which () returns uint32

=head3 state

    method state () returns uint32

=head3 x

    method x () returns int32

=head3 y

    method y () returns int32

=head3 xrel

    method xrel () returns int32

=head3 yrel

    method yrel () returns int32

=head2 SDL::MouseButtonEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 windowID

    method windowID () returns uint32

=head3 which

    method which () returns uint32

=head3 button

    method button () returns uint8

=head3 state

    method state () returns uint8

=head3 clicks

    method clicks () returns uint8

=head3 x

    method x () returns int32

=head3 y

    method y () returns int32

=head2 SDL::MouseWheelEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 windowID

    method windowID () returns uint32

=head3 which

    method which () returns uint32

=head3 x

    method x () returns int32

=head3 y

    method y () returns int32

=head3 direction

    method direction () returns int32

=head2 SDL::ControllerAxisEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 which

    method which () returns uint8

=head3 axis

    method axis () returns uint8

=head3 value

    method value () returns uint16

=head2 SDL::JoyAxisEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 which

    method which () returns uint8

=head3 axis

    method axis () returns uint8

=head3 value

    method value () returns uint16

=head2 SDL::ControllerButtonEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 which

    method which () returns int32

=head3 button

    method button () returns uint8

=head3 state

    method state () returns uint8

=head2 SDL::ControllerDeviceEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 which

    method which () returns int32

=head2 SDL::ControllerTouchpadEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 which

    method which () returns int32

=head3 touchpad

    method touchpad () returns int32

=head3 finger

    method finger () returns int32

=head3 x

    method x () returns num32

=head3 y

    method y () returns num32

=head3 pressure

    method pressure () returns num32

=head2 SDL::ControllerWindowEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 window-id

    method which () returns int32

=head3 event

    method event () returns uint8

=head3 data1

    method data1 () returns int32

=head3 data2

    method data2 () returns int32

=head2 SDL::DropEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 file

    method file () returns Str

=head3 window-id

    method which () returns int32

=head2 SDL::TextInputEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 window-id

    method which () returns uint32

=head3 text

    method text () returns Str

=head2 SDL::TextEditingEvent

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 window-id

    method which () returns uint32

=head3 text

    method text () returns Str

=head3 start

    method start () returns int32

=head3 length

    method length () returns int32

=head2 SDL::Event

=head3 type

    method type () returns uint32

=head3 timestamp

    method timestamp () returns uint32

=head3 btton

    method button () returns SDL::MouseButtonEvent

=head3 caxis

    method caxis () returns SDL::ControllerAxisEvent

=head3 cbutton

    method cbutton () returns SDL::ControllerButtonEvent

=head3 cdevice

    method cdevice () returns SDL::ControllerDeviceEvent

=head3 ctouchpad

    method ctouchpad () returns SDL:: ControllerTouchpadEvent

=head3 drop

    method drop () returns SDL::DropEvent

=head3 edit

    method edit () returns SDL::TextEditingEvent

=head3 key

    method key () returns SDL::KeyboardEvent

=head3 motion

    method motion () returns SDL::MouseMotionEvent

=head3 text

    method text () returns SDL::TextInputEvent

=head3 wheel

    method wheel () returns SDL::MouseWheelEvent

=head3 window

    method window () returns SDL::WindowEvent

=head2 SDL::Window

=head2 SDL::Texture

=head2 SDL::Renderer

=head2 SDL::Joystick

=head2 SDL::GameController

=head2 SDL::Surface

=head3 w

    method w () returns int32

=head3 h

    method h () returns int32

=head3 pitch

    method pitch () returns int32

=head1 FUNCTIONS

=head2 GetError

    sub GetError () returns Str

=head2 GetScancodeFromKey

    sub GetScancodeFromKey (
        int32 $key,
    ) returns int32

=head2 EventState

    sub EventState (
        int32 $type,
        int32 $state,
    ) returns uint8

=head2 PollEvent

    sub PollEvent (
        SDL::Event $event,
    ) returns int32

=head2 PushEvent

    sub PushEvent (
        SDL::Event $event,
    ) returns int32

=head2 RegisterEvents

    sub RegisterEvents (
        int32 $numevents,
    ) returns int32

=head2 Init

    sub Init (
        int32 $flags,
    ) returns int32

=head2 WasInit

    sub WasInit (
        uint32 $flags,
    ) returns uint32

=head2 InitSubsystem

    sub InitSubSystem (
        uint32 $flags
    ) returns uint32

=head2 Delay

    sub Delay (
        uint32 $ms,
    ) returns Nil

=head2 CreateRenderer

    sub CreateRenderer (
        SDL::Window $win,
        int32       $index,
        int32       $flags,
    ) returns SDL::Renderer

=head2 DestroyRenderer

    sub DestroyRenderer (
        SDL::Renderer $renderer,
    ) returns Nil

=head2 CreateTexture

    sub CreateTexture (
        SDL::Renderer $renderer,
        int32 $format,
        int32 $access,
        int32 $w,
        int32 $h,
    ) returns SDL::Texture

=head2 DestroyTexture

    sub DestroyTexture (
        SDL::Texture $texture,
    ) returns Nil

=head RenderClear

    sub RenderClear (
        SDL::Renderer $renderer,
    ) returns int32

=head2 RenderPresent

    sub RenderPresent (
        SDL::Renderer $renderer,
    ) returns int32

=head2 RenderCopy

    sub RenderCopy (
        SDL::Renderer $renderer,
        SDL::Texture  $src,
        SDL::Rect     $srcrect,
        SDL::Rect     $destrect,
    ) returns int32

=head2 RenderCopyEx

    sub RenderCopyEx (
        SDL::Renderer $renderer,
        SDL::Texture  $src,
        SDL::Rect     $srcrect,
        SDL::Rect     $destrect,
        num64         $angle,
        SDL::Point    $center,
        int32         $flip,
    ) returns int32

=head2 RenderSetLogicalSize

    sub RenderSetLogicalSize (
        SDL::Renderer $renderer,
        int32         $w,
        int32         $h,
    ) returns int32

=head2 RenderGetLogicalSize

    sub RenderGetLogicalSize (
        SDL::Renderer  $renderer,
        Pointer[int32] $w,
        Pointer[int32] $h,
    ) returns Nil

=head2 SetRenderTarget

    sub SetRenderTarget (
        SDL::Renderer $renderer,
        SDL::Texture  $texture,
    ) returns int32

=head2 SetRenderDrawColor

    sub SetRenderDrawColor (
        SDL::Renderer $renderer,
        int8          $r,
        int8          $g,
        int8          $b,
        int8          $a
    ) returns int32

=head2 SetTextureBlendMode

    sub SetTextureBlendMode (
        SDL::Texture $texture,
        int32        $blendmode,
    ) returns int32

=head2 GetTextureBlendMode

    sub GetTextureBlendMode (
        SDL::Texture   $texture,
        Pointer[int32] $blendmode,
    ) returns int32

=head2 SetTextureColorMod

    sub SetTextureColorMod (
        SDL::Texture $texture,
        uint8 $r,
        uint8 $g,
        uint8 $b,
    ) returns int32

=head2 GetTextureColorMod

    sub GetTextureColorMod (
        SDL::Texture $texture,
        uint8 $r is rw,
        uint8 $g is rw,
        uint8 $b is rw,
    ) returns int32

=head2 QueryTexture

    sub QueryTexture (
        SDL::Texture $texture,
        uint32       $format is rw,
        int32        $access is rw,
        int32        $w      is rw,
        int32        $h      is rw,
    ) returns int32

=head2 NumJoystics

    sub NumJoysticks () returns int32

=head2 JoystickOpen

    sub JoystickOpen(
        int32 $idx,
    ) returns SDL::Joystick

=head2 JoystickClose

    sub JoystickClose(
        SDL::Joystick $joystick,
    ) returns Nil

=head2 JoystickOpened

    sub JoystickOpened(
        int32 $idx,
    ) returns int32

=head2 JoystickEventState

    sub JoystickEventState(
        int32 $toggle,
    ) returns int32

=head2 JoystickInstanceID

    sub JoystickInstanceID(
        SDL::Joystick $joystick,
    ) returns int32

=head2 JoystickGetAttached

    sub JoystickGetAttached (
        SDL::Joystick $joystick,
    ) returns Bool

=head2 GameControllerGetJoystick

    sub GameControllerGetJoystick (
        SDL::GameController $controller,
    ) returns SDL::Joystick

=head2 GameControllerOpen

    sub GameControllerOpen (
        uint8 $index,
    ) returns SDL::GameController

=head2 GameControllerClose

    sub GameControllerClose (
        SDL::GameController $controller,
    ) returns Nil

=head2 IsGameController

    sub IsGameController (
        uint8 $index,
    ) returns Bool

=head2 GameControllerNameForIndex

    sub GameControllerNameForIndex (
        uint8 $index,
    ) returns str

=head2 CreateRGBSurface

    sub CreateRGBSurface (
        uint32 $flags,
         int32 $width,
         int32 $height,
         int32 $depth,
        uint32 $rmask,
        uint32 $gmask,
        uint32 $bmask,
        uint32 $amask,
    ) returns SDL::Surface

=head2 SetSurfaceColorMod

    sub SetSurfaceColorMod (
        SDL::Surface $src,
        uint8 $r,
        uint8 $g,
        uint8 $b.
    ) returns int32

=head2 GetSurfaceColorMod

    sub GetSurfaceColorMod (
        SDL::Surface $src,
        uint8 $r is rw,
        uint8 $g is rw,
        uint8 $b is rw,
    ) returns int32

=head2 LockSurface

    sub LockSurface (
        SDL::Surface $src,
    ) returns int32

=head2 UnlockSurface

    sub UnlockSurface (
        SDL::Surface $src,
    ) returns Nil

=head2 BlitSurface

    sub BlitSurface (
        SDL::Surface $src,
        SDL::Rect    $srcrect is rw,
        SDL::Surface $dst,
        SDL::Rect    $dstrect is rw,
    ) returns int32

=head2 CreateTextureFromSurface

    sub CreateTextureFromSurface(
        SDL::Renderer $renderer,
        SDL::Surface  $surface,
    ) returns SDL::Texture

=head2 FreeSurface

    sub FreeSurface (
        SDL::Surface $surface,
    ) returns Nil

=head2 SetTextInputRect

    sub SetTextInputRect (
        SDL::Rect $rect is rw,
    ) returns Nil

=head2 CreateWindow

    sub CreateWindow (
        Str   $title,
        int32 $x,
        int32 $y,
        int32 $w,
        int32 $h,
        int32 $flags
    ) returns SDL::Window

=head2 DestroyWindow

    sub DestroyWindow (
        SDL::Window $win,
    ) returns Nil

=head2 ShowCursor

    sub ShowCursor (
        int32 $toggle,
    ) returns int32

=head GetWindowPixelFormat

    sub GetWindowPixelFormat (
        SDL::Window $window,
    ) returns uint32

=head SetHint

    sub SetHint (
        Str $name,
        Str $value,
    ) returns int32

=head1 ENUMS

=head2 SDL::Scancode

=item SDL::SCANCODE_UNKNOWN
=item SDL::SCANCODE_A
=item SDL::SCANCODE_B
=item SDL::SCANCODE_C
=item SDL::SCANCODE_D
=item SDL::SCANCODE_E
=item SDL::SCANCODE_F
=item SDL::SCANCODE_G
=item SDL::SCANCODE_H
=item SDL::SCANCODE_I
=item SDL::SCANCODE_J
=item SDL::SCANCODE_K
=item SDL::SCANCODE_L
=item SDL::SCANCODE_M
=item SDL::SCANCODE_N
=item SDL::SCANCODE_O
=item SDL::SCANCODE_P
=item SDL::SCANCODE_Q
=item SDL::SCANCODE_R
=item SDL::SCANCODE_S
=item SDL::SCANCODE_T
=item SDL::SCANCODE_U
=item SDL::SCANCODE_V
=item SDL::SCANCODE_W
=item SDL::SCANCODE_X
=item SDL::SCANCODE_Y
=item SDL::SCANCODE_Z
=item SDL::SCANCODE_1
=item SDL::SCANCODE_2
=item SDL::SCANCODE_3
=item SDL::SCANCODE_4
=item SDL::SCANCODE_5
=item SDL::SCANCODE_6
=item SDL::SCANCODE_7
=item SDL::SCANCODE_8
=item SDL::SCANCODE_9
=item SDL::SCANCODE_0
=item SDL::SCANCODE_RETURN
=item SDL::SCANCODE_ESCAPE
=item SDL::SCANCODE_BACKSPACE
=item SDL::SCANCODE_TAB
=item SDL::SCANCODE_SPACE
=item SDL::SCANCODE_MINUS
=item SDL::SCANCODE_EQUALS
=item SDL::SCANCODE_LEFTBRACKET
=item SDL::SCANCODE_RIGHTBRACKET
=item SDL::SCANCODE_BACKSLASH
=item SDL::SCANCODE_NONUSHASH
=item SDL::SCANCODE_SEMICOLON
=item SDL::SCANCODE_APOSTROPHE
=item SDL::SCANCODE_GRAVE
=item SDL::SCANCODE_COMMA
=item SDL::SCANCODE_PERIOD
=item SDL::SCANCODE_SLASH
=item SDL::SCANCODE_CAPSLOCK
=item SDL::SCANCODE_F1
=item SDL::SCANCODE_F2
=item SDL::SCANCODE_F3
=item SDL::SCANCODE_F4
=item SDL::SCANCODE_F5
=item SDL::SCANCODE_F6
=item SDL::SCANCODE_F7
=item SDL::SCANCODE_F8
=item SDL::SCANCODE_F9
=item SDL::SCANCODE_F10
=item SDL::SCANCODE_F11
=item SDL::SCANCODE_F12
=item SDL::SCANCODE_PRINTSCREEN
=item SDL::SCANCODE_SCROLLLOCK
=item SDL::SCANCODE_PAUSE
=item SDL::SCANCODE_INSERT
=item SDL::SCANCODE_HOME
=item SDL::SCANCODE_PAGEUP
=item SDL::SCANCODE_DELETE
=item SDL::SCANCODE_END
=item SDL::SCANCODE_PAGEDOWN
=item SDL::SCANCODE_RIGHT
=item SDL::SCANCODE_LEFT
=item SDL::SCANCODE_DOWN
=item SDL::SCANCODE_UP
=item SDL::SCANCODE_NUMLOCKCLEAR
=item SDL::SCANCODE_KP_DIVIDE
=item SDL::SCANCODE_KP_MULTIPLY
=item SDL::SCANCODE_KP_MINUS
=item SDL::SCANCODE_KP_PLUS
=item SDL::SCANCODE_KP_ENTER
=item SDL::SCANCODE_KP_1
=item SDL::SCANCODE_KP_2
=item SDL::SCANCODE_KP_3
=item SDL::SCANCODE_KP_4
=item SDL::SCANCODE_KP_5
=item SDL::SCANCODE_KP_6
=item SDL::SCANCODE_KP_7
=item SDL::SCANCODE_KP_8
=item SDL::SCANCODE_KP_9
=item SDL::SCANCODE_KP_0
=item SDL::SCANCODE_KP_PERIOD
=item SDL::SCANCODE_NONUSBACKSLASH
=item SDL::SCANCODE_APPLICATION
=item SDL::SCANCODE_POWER
=item SDL::SCANCODE_KP_EQUALS
=item SDL::SCANCODE_F13
=item SDL::SCANCODE_F14
=item SDL::SCANCODE_F15
=item SDL::SCANCODE_F16
=item SDL::SCANCODE_F17
=item SDL::SCANCODE_F18
=item SDL::SCANCODE_F19
=item SDL::SCANCODE_F20
=item SDL::SCANCODE_F21
=item SDL::SCANCODE_F22
=item SDL::SCANCODE_F23
=item SDL::SCANCODE_F24
=item SDL::SCANCODE_EXECUTE
=item SDL::SCANCODE_HELP
=item SDL::SCANCODE_MENU
=item SDL::SCANCODE_SELECT
=item SDL::SCANCODE_STOP
=item SDL::SCANCODE_AGAIN
=item SDL::SCANCODE_UNDO
=item SDL::SCANCODE_CUT
=item SDL::SCANCODE_COPY
=item SDL::SCANCODE_PASTE
=item SDL::SCANCODE_FIND
=item SDL::SCANCODE_MUTE
=item SDL::SCANCODE_VOLUMEUP
=item SDL::SCANCODE_VOLUMEDOWN
=item SDL::SCANCODE_KP_COMMA
=item SDL::SCANCODE_KP_EQUALSAS400
=item SDL::SCANCODE_INTERNATIONAL1
=item SDL::SCANCODE_INTERNATIONAL2
=item SDL::SCANCODE_INTERNATIONAL3
=item SDL::SCANCODE_INTERNATIONAL4
=item SDL::SCANCODE_INTERNATIONAL5
=item SDL::SCANCODE_INTERNATIONAL6
=item SDL::SCANCODE_INTERNATIONAL7
=item SDL::SCANCODE_INTERNATIONAL8
=item SDL::SCANCODE_INTERNATIONAL9
=item SDL::SCANCODE_LANG1
=item SDL::SCANCODE_LANG2
=item SDL::SCANCODE_LANG3
=item SDL::SCANCODE_LANG4
=item SDL::SCANCODE_LANG5
=item SDL::SCANCODE_LANG6
=item SDL::SCANCODE_LANG7
=item SDL::SCANCODE_LANG8
=item SDL::SCANCODE_LANG9
=item SDL::SCANCODE_ALTERASE
=item SDL::SCANCODE_SYSREQ
=item SDL::SCANCODE_CANCEL
=item SDL::SCANCODE_CLEAR
=item SDL::SCANCODE_PRIOR
=item SDL::SCANCODE_RETURN2
=item SDL::SCANCODE_SEPARATOR
=item SDL::SCANCODE_OUT
=item SDL::SCANCODE_OPER
=item SDL::SCANCODE_CLEARAGAIN
=item SDL::SCANCODE_CRSEL
=item SDL::SCANCODE_EXSEL
=item SDL::SCANCODE_KP_00
=item SDL::SCANCODE_KP_000
=item SDL::SCANCODE_THOUSANDSSEPARATOR
=item SDL::SCANCODE_DECIMALSEPARATOR
=item SDL::SCANCODE_CURRENCYUNIT
=item SDL::SCANCODE_CURRENCYSUBUNIT
=item SDL::SCANCODE_KP_LEFTPAREN
=item SDL::SCANCODE_KP_RIGHTPAREN
=item SDL::SCANCODE_KP_LEFTBRACE
=item SDL::SCANCODE_KP_RIGHTBRACE
=item SDL::SCANCODE_KP_TAB
=item SDL::SCANCODE_KP_BACKSPACE
=item SDL::SCANCODE_KP_A
=item SDL::SCANCODE_KP_B
=item SDL::SCANCODE_KP_C
=item SDL::SCANCODE_KP_D
=item SDL::SCANCODE_KP_E
=item SDL::SCANCODE_KP_F
=item SDL::SCANCODE_KP_XOR
=item SDL::SCANCODE_KP_POWER
=item SDL::SCANCODE_KP_PERCENT
=item SDL::SCANCODE_KP_LESS
=item SDL::SCANCODE_KP_GREATER
=item SDL::SCANCODE_KP_AMPERSAND
=item SDL::SCANCODE_KP_DBLAMPERSAND
=item SDL::SCANCODE_KP_VERTICALBAR
=item SDL::SCANCODE_KP_DBLVERTICALBAR
=item SDL::SCANCODE_KP_COLON
=item SDL::SCANCODE_KP_HASH
=item SDL::SCANCODE_KP_SPACE
=item SDL::SCANCODE_KP_AT
=item SDL::SCANCODE_KP_EXCLAM
=item SDL::SCANCODE_KP_MEMSTORE
=item SDL::SCANCODE_KP_MEMRECALL
=item SDL::SCANCODE_KP_MEMCLEAR
=item SDL::SCANCODE_KP_MEMADD
=item SDL::SCANCODE_KP_MEMSUBTRACT
=item SDL::SCANCODE_KP_MEMMULTIPLY
=item SDL::SCANCODE_KP_MEMDIVIDE
=item SDL::SCANCODE_KP_PLUSMINUS
=item SDL::SCANCODE_KP_CLEAR
=item SDL::SCANCODE_KP_CLEARENTRY
=item SDL::SCANCODE_KP_BINARY
=item SDL::SCANCODE_KP_OCTAL
=item SDL::SCANCODE_KP_DECIMAL
=item SDL::SCANCODE_KP_HEXADECIMAL
=item SDL::SCANCODE_LCTRL
=item SDL::SCANCODE_LSHIFT
=item SDL::SCANCODE_LALT
=item SDL::SCANCODE_LGUI
=item SDL::SCANCODE_RCTRL
=item SDL::SCANCODE_RSHIFT
=item SDL::SCANCODE_RALT
=item SDL::SCANCODE_RGUI
=item SDL::SCANCODE_MODE
=item SDL::SCANCODE_AUDIONEXT
=item SDL::SCANCODE_AUDIOPREV
=item SDL::SCANCODE_AUDIOSTOP
=item SDL::SCANCODE_AUDIOPLAY
=item SDL::SCANCODE_AUDIOMUTE
=item SDL::SCANCODE_MEDIASELECT
=item SDL::SCANCODE_WWW
=item SDL::SCANCODE_MAIL
=item SDL::SCANCODE_CALCULATOR
=item SDL::SCANCODE_COMPUTER
=item SDL::SCANCODE_AC_SEARCH
=item SDL::SCANCODE_AC_HOME
=item SDL::SCANCODE_AC_BACK
=item SDL::SCANCODE_AC_FORWARD
=item SDL::SCANCODE_AC_STOP
=item SDL::SCANCODE_AC_REFRESH
=item SDL::SCANCODE_AC_BOOKMARKS
=item SDL::SCANCODE_BRIGHTNESSDOWN
=item SDL::SCANCODE_BRIGHTNESSUP
=item SDL::SCANCODE_DISPLAYSWITCH
=item SDL::SCANCODE_KBDILLUMTOGGLE
=item SDL::SCANCODE_KBDILLUMDOWN
=item SDL::SCANCODE_KBDILLUMUP
=item SDL::SCANCODE_EJECT
=item SDL::SCANCODE_SLEEP
=item SDL::SCANCODE_APP1
=item SDL::SCANCODE_APP2
=item SDL::SCANCODE_AUDIOREWIND
=item SDL::SCANCODE_AUDIOFASTFORWARD

=head2 SDL::Keycode

=item SDL::K_UNKNOWN
=item SDL::K_RETURN
=item SDL::K_ESCAPE
=item SDL::K_BACKSPACE
=item SDL::K_TAB
=item SDL::K_SPACE
=item SDL::K_EXCLAIM
=item SDL::K_QUOTEDBL
=item SDL::K_HASH
=item SDL::K_PERCENT
=item SDL::K_DOLLAR
=item SDL::K_AMPERSAND
=item SDL::K_QUOTE
=item SDL::K_LEFTPAREN
=item SDL::K_RIGHTPAREN
=item SDL::K_ASTERISK
=item SDL::K_PLUS
=item SDL::K_COMMA
=item SDL::K_MINUS
=item SDL::K_PERIOD
=item SDL::K_SLASH
=item SDL::K_0
=item SDL::K_1
=item SDL::K_2
=item SDL::K_3
=item SDL::K_4
=item SDL::K_5
=item SDL::K_6
=item SDL::K_7
=item SDL::K_8
=item SDL::K_9
=item SDL::K_COLON
=item SDL::K_SEMICOLON
=item SDL::K_LESS
=item SDL::K_EQUALS
=item SDL::K_GREATER
=item SDL::K_QUESTION
=item SDL::K_AT
=item SDL::K_LEFTBRACKET
=item SDL::K_BACKSLASH
=item SDL::K_RIGHTBRACKET
=item SDL::K_CARET
=item SDL::K_UNDERSCORE
=item SDL::K_BACKQUOTE
=item SDL::K_a
=item SDL::K_b
=item SDL::K_c
=item SDL::K_d
=item SDL::K_e
=item SDL::K_f
=item SDL::K_g
=item SDL::K_h
=item SDL::K_i
=item SDL::K_j
=item SDL::K_k
=item SDL::K_l
=item SDL::K_m
=item SDL::K_n
=item SDL::K_o
=item SDL::K_p
=item SDL::K_q
=item SDL::K_r
=item SDL::K_s
=item SDL::K_t
=item SDL::K_u
=item SDL::K_v
=item SDL::K_w
=item SDL::K_x
=item SDL::K_y
=item SDL::K_z
=item SDL::K_CAPSLOCK
=item SDL::K_F1
=item SDL::K_F2
=item SDL::K_F3
=item SDL::K_F4
=item SDL::K_F5
=item SDL::K_F6
=item SDL::K_F7
=item SDL::K_F8
=item SDL::K_F9
=item SDL::K_F10
=item SDL::K_F11
=item SDL::K_F12
=item SDL::K_PRINTSCREEN
=item SDL::K_SCROLLLOCK
=item SDL::K_PAUSE
=item SDL::K_INSERT
=item SDL::K_HOME
=item SDL::K_PAGEUP
=item SDL::K_DELETE
=item SDL::K_END
=item SDL::K_PAGEDOWN
=item SDL::K_RIGHT
=item SDL::K_LEFT
=item SDL::K_DOWN
=item SDL::K_UP
=item SDL::K_NUMLOCKCLEAR
=item SDL::K_KP_DIVIDE
=item SDL::K_KP_MULTIPLY
=item SDL::K_KP_MINUS
=item SDL::K_KP_PLUS
=item SDL::K_KP_ENTER
=item SDL::K_KP_1
=item SDL::K_KP_2
=item SDL::K_KP_3
=item SDL::K_KP_4
=item SDL::K_KP_5
=item SDL::K_KP_6
=item SDL::K_KP_7
=item SDL::K_KP_8
=item SDL::K_KP_9
=item SDL::K_KP_0
=item SDL::K_KP_PERIOD
=item SDL::K_APPLICATION
=item SDL::K_POWER
=item SDL::K_KP_EQUALS
=item SDL::K_F13
=item SDL::K_F14
=item SDL::K_F15
=item SDL::K_F16
=item SDL::K_F17
=item SDL::K_F18
=item SDL::K_F19
=item SDL::K_F20
=item SDL::K_F21
=item SDL::K_F22
=item SDL::K_F23
=item SDL::K_F24
=item SDL::K_EXECUTE
=item SDL::K_HELP
=item SDL::K_MENU
=item SDL::K_SELECT
=item SDL::K_STOP
=item SDL::K_AGAIN
=item SDL::K_UNDO
=item SDL::K_CUT
=item SDL::K_COPY
=item SDL::K_PASTE
=item SDL::K_FIND
=item SDL::K_MUTE
=item SDL::K_VOLUMEUP
=item SDL::K_VOLUMEDOWN
=item SDL::K_KP_COMMA
=item SDL::K_KP_EQUALSAS400
=item SDL::K_ALTERASE
=item SDL::K_SYSREQ
=item SDL::K_CANCEL
=item SDL::K_CLEAR
=item SDL::K_PRIOR
=item SDL::K_RETURN2
=item SDL::K_SEPARATOR
=item SDL::K_OUT
=item SDL::K_OPER
=item SDL::K_CLEARAGAIN
=item SDL::K_CRSEL
=item SDL::K_EXSEL
=item SDL::K_KP_00
=item SDL::K_KP_000
=item SDL::K_THOUSANDSSEPARATOR
=item SDL::K_DECIMALSEPARATOR
=item SDL::K_CURRENCYUNIT
=item SDL::K_CURRENCYSUBUNIT
=item SDL::K_KP_LEFTPAREN
=item SDL::K_KP_RIGHTPAREN
=item SDL::K_KP_LEFTBRACE
=item SDL::K_KP_RIGHTBRACE
=item SDL::K_KP_TAB
=item SDL::K_KP_BACKSPACE
=item SDL::K_KP_A
=item SDL::K_KP_B
=item SDL::K_KP_C
=item SDL::K_KP_D
=item SDL::K_KP_E
=item SDL::K_KP_F
=item SDL::K_KP_XOR
=item SDL::K_KP_POWER
=item SDL::K_KP_PERCENT
=item SDL::K_KP_LESS
=item SDL::K_KP_GREATER
=item SDL::K_KP_AMPERSAND
=item SDL::K_KP_DBLAMPERSAND
=item SDL::K_KP_VERTICALBAR
=item SDL::K_KP_DBLVERTICALBAR
=item SDL::K_KP_COLON
=item SDL::K_KP_HASH
=item SDL::K_KP_SPACE
=item SDL::K_KP_AT
=item SDL::K_KP_EXCLAM
=item SDL::K_KP_MEMSTORE
=item SDL::K_KP_MEMRECALL
=item SDL::K_KP_MEMCLEAR
=item SDL::K_KP_MEMADD
=item SDL::K_KP_MEMSUBTRACT
=item SDL::K_KP_MEMMULTIPLY
=item SDL::K_KP_MEMDIVIDE
=item SDL::K_KP_PLUSMINUS
=item SDL::K_KP_CLEAR
=item SDL::K_KP_CLEARENTRY
=item SDL::K_KP_BINARY
=item SDL::K_KP_OCTAL
=item SDL::K_KP_DECIMAL
=item SDL::K_KP_HEXADECIMAL
=item SDL::K_LCTRL
=item SDL::K_LSHIFT
=item SDL::K_LALT
=item SDL::K_LGUI
=item SDL::K_RCTRL
=item SDL::K_RSHIFT
=item SDL::K_RALT
=item SDL::K_RGUI
=item SDL::K_MODE
=item SDL::K_AUDIONEXT
=item SDL::K_AUDIOPREV
=item SDL::K_AUDIOSTOP
=item SDL::K_AUDIOPLAY
=item SDL::K_AUDIOMUTE
=item SDL::K_MEDIASELECT
=item SDL::K_WWW
=item SDL::K_MAIL
=item SDL::K_CALCULATOR
=item SDL::K_COMPUTER
=item SDL::K_AC_SEARCH
=item SDL::K_AC_HOME
=item SDL::K_AC_BACK
=item SDL::K_AC_FORWARD
=item SDL::K_AC_STOP
=item SDL::K_AC_REFRESH
=item SDL::K_AC_BOOKMARKS
=item SDL::K_BRIGHTNESSDOWN
=item SDL::K_BRIGHTNESSUP
=item SDL::K_DISPLAYSWITCH
=item SDL::K_KBDILLUMTOGGLE
=item SDL::K_KBDILLUMDOWN
=item SDL::K_KBDILLUMUP
=item SDL::K_EJECT
=item SDL::K_SLEEP
=item SDL::K_APP1
=item SDL::K_APP2
=item SDL::K_AUDIOREWIND
=item SDL::K_AUDIOFASTFORWARD

=head2 SDL::EventType

=item SDL::QUIT
=item SDL::APP_TERMINATING
=item SDL::APP_LOWMEMORY
=item SDL::APP_WILLENTERBACKGROUND
=item SDL::APP_DIDENTERBACKGROUND
=item SDL::APP_WILLENTERFOREGROUND
=item SDL::APP_DIDENTERFOREGROUND
=item SDL::LOCALECHANGED
=item SDL::DISPLAYEVENT
=item SDL::WINDOWEVENT
=item SDL::SYSWMEVENT
=item SDL::KEYDOWN
=item SDL::KEYUP
=item SDL::TEXTEDITING
=item SDL::TEXTINPUT
=item SDL::KEYMAPCHANGED
=item SDL::MOUSEMOTION
=item SDL::MOUSEBUTTONDOWN
=item SDL::MOUSEBUTTONUP
=item SDL::MOUSEWHEEL
=item SDL::JOYAXISMOTION
=item SDL::JOYBALLMOTION
=item SDL::JOYHATMOTION
=item SDL::JOYBUTTONDOWN
=item SDL::JOYBUTTONUP
=item SDL::JOYDEVICEADDED
=item SDL::JOYDEVICEREMOVED
=item SDL::CONTROLLERAXISMOTION
=item SDL::CONTROLLERBUTTONDOWN
=item SDL::CONTROLLERBUTTONUP
=item SDL::CONTROLLERDEVICEADDED
=item SDL::CONTROLLERDEVICEREMOVED
=item SDL::CONTROLLERDEVICEREMAPPE
=item SDL::CONTROLLERTOUCHPADDOWN
=item SDL::CONTROLLERTOUCHPADMOTION
=item SDL::CONTROLLERTOUCHPADUP
=item SDL::CONTROLLERSENSORUPDATE
=item SDL::FINGERDOWN
=item SDL::FINGERUP
=item SDL::FINGERMOTION
=item SDL::DOLLARGESTURE
=item SDL::DOLLARRECORD
=item SDL::MULTIGESTURE
=item SDL::CLIPBOARDUPDATE
=item SDL::DROPFILE
=item SDL::DROPTEXT
=item SDL::DROPBEGIN
=item SDL::DROPCOMPLETE
=item SDL::AUDIODEVICEADDED
=item SDL::AUDIODEVICEREMOVED
=item SDL::SENSORUPDATE
=item SDL::RENDER_TARGETS_RESET
=item SDL::RENDER_DEVICE_RESET

=head2 SDL::WindowEventType

=item SDL::WINDOWEVENT_NONE
=item SDL::WINDOWEVENT_SHOWN
=item SDL::WINDOWEVENT_HIDDEN
=item SDL::WINDOWEVENT_EXPOSED
=item SDL::WINDOWEVENT_MOVED
=item SDL::WINDOWEVENT_RESIZED
=item SDL::WINDOWEVENT_SIZE_CHANGED
=item SDL::WINDOWEVENT_MINIMIZED
=item SDL::WINDOWEVENT_MAXIMIZED
=item SDL::WINDOWEVENT_RESTORED
=item SDL::WINDOWEVENT_ENTER
=item SDL::WINDOWEVENT_LEAVE
=item SDL::WINDOWEVENT_FOCUS_GAINED
=item SDL::WINDOWEVENT_FOCUS_LOST
=item SDL::WINDOWEVENT_CLOSE
=item SDL::WINDOWEVENT_TAKE_FOCUS
=item SDL::WINDOWEVENT_HIT_TEST

=head2 SDL::MouseButton

=item SDL::BUTTON_LEFT
=item SDL::BUTTON_MIDDLE
=item SDL::BUTTON_RIGHT
=item SDL::BUTTON_X1
=item SDL::BUTTON_X2

=head2 SDL::TextureAccess

=item SDL::TEXTUREACCESS_STATIC
=item SDL::TEXTUREACCESS_STREAMING
=item SDL::TEXTUREACCESS_TARGET

=head2 SDL::RendererFlip

=item SDL::FLIP_NONE
=item SDL::FLIP_HORIZONTAL
=item SDL::FLIP_VERTICAL

=head2 SDL::BlendMode

=item SDL::BLENDMODE_NONE
=item SDL::BLENDMODE_BLEND
=item SDL::BLENDMODE_ADD
=item SDL::BLENDMODE_MOD
=item SDL::BLENDMODE_MUL
=item SDL::BLENDMODE_INVALID

=head2 SDL::GameControllerType

=item SDL::CONTROLLER_TYPE_UNKNOWN
=item SDL::CONTROLLER_TYPE_XBOX360
=item SDL::CONTROLLER_TYPE_XBOXONE
=item SDL::CONTROLLER_TYPE_PS3
=item SDL::CONTROLLER_TYPE_PS4
=item SDL::CONTROLLER_TYPE_NINTENDO_SWITCH_PRO
=item SDL::CONTROLLER_TYPE_VIRTUAL
=item SDL::CONTROLLER_TYPE_PS5

=head2 SDL::GameControllerBindType

=item SDL::CONTROLLER_BINDTYPE_NONE
=item SDL::CONTROLLER_BINDTYPE_BUTTON
=item SDL::CONTROLLER_BINDTYPE_AXIS
=item SDL::CONTROLLER_BINDTYPE_HAT

=head2 SDL::GameControllerAxis

=item SDL::CONTROLLER_AXIS_INVALID
=item SDL::CONTROLLER_AXIS_LEFTX
=item SDL::CONTROLLER_AXIS_LEFTY
=item SDL::CONTROLLER_AXIS_RIGHTX
=item SDL::CONTROLLER_AXIS_RIGHTY
=item SDL::CONTROLLER_AXIS_TRIGGERLEFT
=item SDL::CONTROLLER_AXIS_TRIGGERRIGHT
=item SDL::CONTROLLER_AXIS_MAX

=head2 SDL::GameControllerButton

=item SDL::CONTROLLER_BUTTON_INVALID
=item SDL::CONTROLLER_BUTTON_A
=item SDL::CONTROLLER_BUTTON_B
=item SDL::CONTROLLER_BUTTON_X
=item SDL::CONTROLLER_BUTTON_Y
=item SDL::CONTROLLER_BUTTON_BACK
=item SDL::CONTROLLER_BUTTON_GUIDE
=item SDL::CONTROLLER_BUTTON_START
=item SDL::CONTROLLER_BUTTON_LEFTSTICK
=item SDL::CONTROLLER_BUTTON_RIGHTSTICK
=item SDL::CONTROLLER_BUTTON_LEFTSHOULDER
=item SDL::CONTROLLER_BUTTON_RIGHTSHOULDER
=item SDL::CONTROLLER_BUTTON_DPAD_UP
=item SDL::CONTROLLER_BUTTON_DPAD_DOWN
=item SDL::CONTROLLER_BUTTON_DPAD_LEFT
=item SDL::CONTROLLER_BUTTON_DPAD_RIGHT
=item SDL::CONTROLLER_BUTTON_MISC1
=item SDL::CONTROLLER_BUTTON_PADDLE1
=item SDL::CONTROLLER_BUTTON_PADDLE2
=item SDL::CONTROLLER_BUTTON_PADDLE3
=item SDL::CONTROLLER_BUTTON_PADDLE4
=item SDL::CONTROLLER_BUTTON_TOUCHPAD
=item SDL::CONTROLLER_BUTTON_MAX

=head2 SDL::Pixeltype

=item SDL::PIXELTYPE_UNKNOWN
=item SDL::PIXELTYPE_INDEX1
=item SDL::PIXELTYPE_INDEX4
=item SDL::PIXELTYPE_INDEX8
=item SDL::PIXELTYPE_PACKED8
=item SDL::PIXELTYPE_PACKED16
=item SDL::PIXELTYPE_PACKED32
=item SDL::PIXELTYPE_ARRAYU8
=item SDL::PIXELTYPE_ARRAYU16
=item SDL::PIXELTYPE_ARRAYU32
=item SDL::PIXELTYPE_ARRAYF16
=item SDL::PIXELTYPE_ARRAYF32

=head2 SDL::WindowFlags

=item SDL::WINDOW_FULLSCREEN
=item SDL::WINDOW_OPENGL
=item SDL::WINDOW_SHOWN
=item SDL::WINDOW_HIDDEN
=item SDL::WINDOW_BORDERLESS
=item SDL::WINDOW_RESIZABLE
=item SDL::WINDOW_MINIMIZED
=item SDL::WINDOW_MAXIMIZED
=item SDL::WINDOW_MOUSE_GRABBED
=item SDL::WINDOW_INPUT_GRABBED
=item SDL::WINDOW_INPUT_FOCUS
=item SDL::WINDOW_MOUSE_FOCUS
=item SDL::WINDOW_FOREIGN
=item SDL::WINDOW_FULLSCREEN_DESKTOP
=item SDL::WINDOW_ALLOW_HIGHDPI
=item SDL::WINDOW_MOUSE_CAPTURE
=item SDL::WINDOW_ALWAYS_ON_TOP
=item SDL::WINDOW_SKIP_TASKBAR
=item SDL::WINDOW_UTILITY
=item SDL::WINDOW_TOOLTIP
=item SDL::WINDOW_POPUP_MENU
=item SDL::WINDOW_KEYBOARD_GRABBED
=item SDL::WINDOW_VULKAN
=item SDL::WINDOW_METAL

=head1 COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

=end pod
