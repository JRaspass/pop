=begin pod

=head1 NAME

Pop::SDL::Image

=head1 SYNOPSIS

    use Pop::SDL::Image;

=head1 DESCRIPTION

Pop::SDL::Image includes a set of custom bindings to the SDL2_image extension
library for SDL2. It aims to provide bindings to a reasonably complete set of
SDL2_image that should be instantly recognisable to someone who is familiar
with SDL2_image itself. This means that this library aims to provide little
if any syntactic sugar at all, and to keep names as close as possible to those
in the original headers.

Names are exported in the SDL::Image Raku namespace, but the names of the
symbols themselves remain unchanged.

The information below serves as an indication of what this library implements.
For specifics, please refer to
L<the SDL2_image documentation|https://www.libsdl.org/projects/SDL_image/docs/SDL_image.html>.

=head1 FUNCTIONS

=head2 Init

    sub Init (
        int32 $flag,
    ) returns int32

=head2 Load

    sub Load (
        Str $path,
    ) returns SDL::Surface

=head2 LoadTexture

    sub LoadTexture (
        SDL::Renderer $renderer,
        Str           $path,
    ) returns SDL::Texture

=head2 GetError

    sub GetError () returns str

=head1 CONSTANTS

=item INIT_JPG
=item INIT_PNG
=item INIT_TIF
=item INIT_WEBP

=head1 COPYRIGHT AND LICENSE

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under
the Artistic License 2.0.

=end pod
