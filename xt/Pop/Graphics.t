#!/usr/bin/env raku

use Test;
use Pop::Graphics;
use Pop::Point;

my ( $width, $height ) = 640, 480;

Pop::Graphics.new( :$width, :$height, title => ~$*PROGRAM );
END Pop::Graphics.destroy;

for (
    # Points
    {
        label 'Points';
        Pop::Graphics.point: random-xy,    random-rgba;
        Pop::Graphics.point: random-point, random-rgba;
    },
    # Lines
    {
        label 'Vertical lines';
        my $p = random-point;
        Pop::Graphics.line: $p.list, $p.add( 0, random-y ).list, random-rgba;
        Pop::Graphics.line: $p,      $p.add( 0, random-y ),      random-rgba;
    },
    {
        label 'Horizontal lines';
        my $p = random-point;
        Pop::Graphics.line: $p.list, $p.add( random-x, 0 ).list, random-rgba;
        Pop::Graphics.line: $p,      $p.add( random-x, 0 ),      random-rgba;
    },
    {
        label 'Arbitrary lines';
        Pop::Graphics.line: random-xy,    random-xy,    random-rgba;
        Pop::Graphics.line: random-point, random-point, random-rgba;
    },
    {
        label 'Arbitrary lines';
        Pop::Graphics.line: random-xy,    random-xy,    random-rgba;
        Pop::Graphics.line: random-point, random-point, random-rgba;
    },
    {
        label 'Thick lines';
        Pop::Graphics.line: width => 1 + (^10).pick, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.line: width => 1 + (^10).pick, random-point, random-point, random-rgba;
    },
    {
        label 'Anti-aliased lines';
        Pop::Graphics.line: :anti-alias, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.line: :anti-alias, random-point, random-point, random-rgba;
    },
    {
        label 'Anti-aliased lines (using :aa)';
        Pop::Graphics.line: :aa, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.line: :aa, random-point, random-point, random-rgba;
    },
    # Rectangles
    {
        label 'Rectangles';
        Pop::Graphics.rectangle: random-xy,    random-xy,    random-rgba;
        Pop::Graphics.rectangle: random-point, random-point, random-rgba;
    },
    {
        label 'Rectangles with rounded corners';
        Pop::Graphics.rectangle: border-radius => random-radius, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.rectangle: border-radius => random-radius, random-point, random-point, random-rgba;
    },
    {
        label 'Filled rectangles';
        Pop::Graphics.rectangle: :fill, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.rectangle: :fill, random-point, random-point, random-rgba;
    },
    {
        label 'Filled rectangles with rounded corners';
        Pop::Graphics.rectangle: :fill, border-radius => random-radius, random-xy,    random-xy,    random-rgba;
        Pop::Graphics.rectangle: :fill, border-radius => random-radius, random-point, random-point, random-rgba;
    },
    # Arcs
    {
        label 'Open arcs';
        Pop::Graphics.arc: random-xy,    random-radius, random-angle, random-angle, random-rgba;
        Pop::Graphics.arc: random-point, random-radius, random-angle, random-angle, random-rgba;
    },
    {
        label 'Pie arcs';
        Pop::Graphics.arc: :pie, random-xy,    random-radius, random-angle, random-angle, random-rgba;
        Pop::Graphics.arc: :pie, random-point, random-radius, random-angle, random-angle, random-rgba;
    },
    {
        label 'Filled arcs (pies)';
        Pop::Graphics.arc:       :fill, random-xy,    random-radius, random-angle, random-angle, random-rgba;
        Pop::Graphics.arc: :pie, :fill, random-point, random-radius, random-angle, random-angle, random-rgba;
    },
    # Circles
    {
        label 'Circles';
        Pop::Graphics.circle: random-xy,    random-radius, random-rgba;
        Pop::Graphics.circle: random-point, random-radius, random-rgba;
    },
    {
        label 'Filled circles';
        Pop::Graphics.circle: :fill, random-xy,    random-radius, random-rgba;
        Pop::Graphics.circle: :fill, random-point, random-radius, random-rgba;
    },
    {
        label 'Anti-aliased circles';
        Pop::Graphics.circle: :anti-alias, random-xy,    random-radius, random-rgba;
        Pop::Graphics.circle: :anti-alias, random-point, random-radius, random-rgba;
    },
    {
        label 'Anti-aliased circles (using :aa)';
        Pop::Graphics.circle: :aa, random-xy,    random-radius, random-rgba;
        Pop::Graphics.circle: :aa, random-point, random-radius, random-rgba;
    },
    # Ellipses
    {
        label 'Ellipses';
        Pop::Graphics.ellipse: random-xy,    ( random-radius, random-radius ), random-rgba;
        Pop::Graphics.ellipse: random-point, ( random-radius, random-radius ), random-rgba;
    },
    {
        label 'Anti-aliased ellipses';
        Pop::Graphics.ellipse: :anti-alias, random-xy,    ( random-radius, random-radius ), random-rgba;
        Pop::Graphics.ellipse: :anti-alias, random-point, ( random-radius, random-radius ), random-rgba;
    },
    {
        label 'Anti-aliased ellipses (using :aa)';
        Pop::Graphics.ellipse: :aa, random-xy,    ( random-radius, random-radius ), random-rgba;
        Pop::Graphics.ellipse: :aa, random-point, ( random-radius, random-radius ), random-rgba;
    },
    {
        label 'Filled ellipses';
        Pop::Graphics.ellipse: :fill, random-xy,    ( random-radius, random-radius ), random-rgba;
        Pop::Graphics.ellipse: :fill, random-point, ( random-radius, random-radius ), random-rgba;
    },
    # Polygons
    {
        label 'Triangles';
        Pop::Graphics.polygon: random-ngon(3),      random-rgba;
        Pop::Graphics.polygon: random-ngon(3, :xy), random-rgba;
    },
    {
        label 'Anti-aliased triangles';
        Pop::Graphics.polygon: :anti-alias, random-ngon(3),      random-rgba;
        Pop::Graphics.polygon: :anti-alias, random-ngon(3, :xy), random-rgba;
    },
    {
        label 'Anti-aliased triangles (using :aa)';
        Pop::Graphics.polygon: :aa, random-ngon(3),      random-rgba;
        Pop::Graphics.polygon: :aa, random-ngon(3, :xy), random-rgba;
    },
    {
        label 'Filled triangles';
        Pop::Graphics.polygon: :fill, random-ngon(3),      random-rgba;
        Pop::Graphics.polygon: :fill, random-ngon(3, :xy), random-rgba;
    },
    {
        label 'Arbitrary polygons';
        Pop::Graphics.polygon: random-ngon,      random-rgba;
        Pop::Graphics.polygon: random-ngon(:xy), random-rgba;
    },
    {
        label 'Anti-aliased arbitrary polygons';
        Pop::Graphics.polygon: :anti-alias, random-ngon,      random-rgba;
        Pop::Graphics.polygon: :anti-alias, random-ngon(:xy), random-rgba;
    },
    {
        label 'Anti-aliased arbitrary polygons (using :aa)';
        Pop::Graphics.polygon: :aa, random-ngon,      random-rgba;
        Pop::Graphics.polygon: :aa, random-ngon(:xy), random-rgba;
    },
    {
        label 'Filled arbitrary polygons';
        Pop::Graphics.polygon: :fill, random-ngon,      random-rgba;
        Pop::Graphics.polygon: :fill, random-ngon(:xy), random-rgba;
    },
    {
        label 'Bezier curvess (default steps)';
        Pop::Graphics.bezier: random-ngon,      random-rgba;
        Pop::Graphics.bezier: random-ngon(:xy), random-rgba;
    },
    {
        label 'Bezier curvess (random steps)';
        Pop::Graphics.bezier: random-ngon,      random-rgba, steps => 2 + Bool.pick;
        Pop::Graphics.bezier: random-ngon(:xy), random-rgba;
    },
) -> &block {
    for ^100 {
        FIRST clear-screen;
        LAST {
            present;
            sleep 1 if %*ENV<INTERACTIVE_TESTING>;
        }
        block();
        ok 1;
    }
}

done-testing;

sub random-ngon ( $n = (^10).pick, :$xy ) {
    return random-xy() xx $n if $xy;
    random-point() xx $n
}

sub label ( $s, $x = 5, $y = 5 ) {
    Pop::Graphics.string: $s, ( $x, $y ), ( 0xFF xx 4 );
}

sub random-rgba { (^256).pick xx 4 }
sub random-angle { ( 0 .. 360 ).pick }
sub random-radius { (^30).pick }
sub random-radians { ( 0 .. 2*π ).pick }
sub random-y { ( 0 .. $height ).pick }
sub random-x { ( 0 .. $width ).pick }
sub random-xy { random-x, random-y }
sub random-point { Pop::Point.new( |random-xy ) }
sub present { Pop::Graphics.present }
sub clear-screen { Pop::Graphics.clear }
