#!/usr/bin/env raku

use Test;
use Pop::Entities;

class A { }
class B { }
class C { }

subtest 'Stress tests' => {
    for 9, 99, 999, 9999 -> $n {
        diag "Testing $n entities";
        LEAVE diag 'Took ' ~ now - ENTER now;

        Pop::Entities.clear;

        for 0 ..^ $n {
            my $mod = $_ % 3;
            with Pop::Entities.create {
                Pop::Entities.add: $_, A;
                Pop::Entities.add: $_, B if $mod;
                Pop::Entities.add: $_, C if $mod == 2;
            }
        }

        for (
                $n     => ( A, ),
            2 * $n / 3 => ( A, B, ),
                $n / 3 => ( B, C, ),
                $n / 3 => ( A, C, ),
                $n / 3 => ( A, B, C, ),
        ) -> ( :key($want), :value(@components) ) {
            diag "Testing { join '-', @components.map(*.^name) }";

            my @array = gather for Pop::Entities.view(|@components) { .take }
            is @array.elems, $want;

            diag 'Took ' ~ now - ENTER now;
        }
    }
}

done-testing;
