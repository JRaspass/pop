#!/usr/bin/env raku

use Test;
use Pop::SDL;
use Pop::SDL::GFX;

if SDL::Init(SDL::INIT_VIDEO) {
    note "Error initialising SDL: { SDL::GetError() }";
}

my $width  = 640;
my $height = 480;

my $window = SDL::CreateWindow(
    ~$*PROGRAM,
    SDL::WINDOWPOS_CENTERED,
    SDL::WINDOWPOS_CENTERED,
    $width,
    $height,
    SDL::WINDOW_OPENGL,
) or note "Error creating SDL window: { SDL::GetError() }";

my $renderer = SDL::CreateRenderer(
    $window,
    -1, # index
    0,  # flags
) or note "Error creating SDL renderer: { SDL::GetError() }";

END {
    SDL::DestroyRenderer($_) with $renderer;
    SDL::DestroyWindow($_) with $window;
}

for (
    {
        SDL::GFX::pixelColor( $renderer, |random-point, random-color );
        SDL::GFX::pixelRGBA(  $renderer, |random-point, |random-rgba );
    },
    {
        SDL::GFX::hlineColor( $renderer, random-x, |random-point, random-color );
        SDL::GFX::hlineRGBA(  $renderer, random-x, |random-point, |random-rgba );
    },
    {
        SDL::GFX::vlineColor( $renderer, |random-point, random-y, random-color );
        SDL::GFX::vlineRGBA(  $renderer, |random-point, random-y, |random-rgba );
    },
    {
        SDL::GFX::rectangleColor( $renderer, |random-point, |random-point, random-color );
        SDL::GFX::rectangleRGBA(  $renderer, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::roundedRectangleColor( $renderer, |random-point, |random-point, random-radius, random-color );
        SDL::GFX::roundedRectangleRGBA(  $renderer, |random-point, |random-point, random-radius, |random-rgba );
    },
    {
        SDL::GFX::boxColor( $renderer, |random-point, |random-point, random-color );
        SDL::GFX::boxRGBA(  $renderer, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::roundedBoxColor( $renderer, |random-point, |random-point, random-radius, random-color );
        SDL::GFX::roundedBoxRGBA(  $renderer, |random-point, |random-point, random-radius, |random-rgba );
    },
    {
        SDL::GFX::lineColor( $renderer, |random-point, |random-point, random-color );
        SDL::GFX::lineRGBA(  $renderer, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::aalineColor( $renderer, |random-point, |random-point, random-color );
        SDL::GFX::aalineRGBA(  $renderer, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::circleColor( $renderer, |random-point, random-radius, random-color );
        SDL::GFX::circleRGBA(  $renderer, |random-point, random-radius, |random-rgba );
    },
    {
        SDL::GFX::arcColor( $renderer, |random-point, random-radius, random-angle, random-angle, random-color );
        SDL::GFX::arcRGBA(  $renderer, |random-point, random-radius, random-angle, random-angle, |random-rgba );
    },
    {
        SDL::GFX::aacircleColor( $renderer, |random-point, random-radius, random-color );
        SDL::GFX::aacircleRGBA(  $renderer, |random-point, random-radius, |random-rgba );
    },
    {
        SDL::GFX::filledCircleColor( $renderer, |random-point, random-radius, random-color );
        SDL::GFX::filledCircleRGBA(  $renderer, |random-point, random-radius, |random-rgba );
    },
    {
        SDL::GFX::ellipseColor( $renderer, |random-point, random-radius, random-radius, random-color );
        SDL::GFX::ellipseRGBA(  $renderer, |random-point, random-radius, random-radius, |random-rgba );
    },
    {
        SDL::GFX::aaellipseColor( $renderer, |random-point, random-radius, random-radius, random-color );
        SDL::GFX::aaellipseRGBA(  $renderer, |random-point, random-radius, random-radius, |random-rgba );
    },
    {
        SDL::GFX::filledEllipseColor( $renderer, |random-point, random-radius, random-radius, random-color );
        SDL::GFX::filledEllipseRGBA(  $renderer, |random-point, random-radius, random-radius, |random-rgba );
    },
    {
        SDL::GFX::pieColor( $renderer, |random-point, random-radius, random-angle, random-angle, random-color );
        SDL::GFX::pieRGBA(  $renderer, |random-point, random-radius, random-angle, random-angle, |random-rgba );
    },
    {
        SDL::GFX::filledPieColor( $renderer, |random-point, random-radius, random-angle, random-angle, random-color );
        SDL::GFX::filledPieRGBA(  $renderer, |random-point, random-radius, random-angle, random-angle, |random-rgba );
    },
    {
        SDL::GFX::trigonColor( $renderer, |random-point, |random-point, |random-point, random-color );
        SDL::GFX::trigonRGBA(  $renderer, |random-point, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::aatrigonColor( $renderer, |random-point, |random-point, |random-point, random-color );
        SDL::GFX::aatrigonRGBA(  $renderer, |random-point, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::filledTrigonColor( $renderer, |random-point, |random-point, |random-point, random-color );
        SDL::GFX::filledTrigonRGBA(  $renderer, |random-point, |random-point, |random-point, |random-rgba );
    },
    {
        SDL::GFX::polygonColor( $renderer, |random-ngon( ( 3 .. 10 ).pick ), random-color );
        SDL::GFX::polygonRGBA(  $renderer, |random-ngon( ( 3 .. 10 ).pick ), |random-rgba );
    },
    {
        SDL::GFX::filledPolygonColor( $renderer, |random-ngon( ( 3 .. 10 ).pick ), random-color );
        SDL::GFX::filledPolygonRGBA(  $renderer, |random-ngon( ( 3 .. 10 ).pick ), |random-rgba );
    },
    {
        SDL::GFX::aapolygonColor( $renderer, |random-ngon( ( 3 .. 10 ).pick ), random-color );
        SDL::GFX::aapolygonRGBA(  $renderer, |random-ngon( ( 3 .. 10 ).pick ), |random-rgba );
    },
    {
        SDL::GFX::bezierColor( $renderer, |random-ngon( ( 3 .. 10 ).pick ), 2, random-color );
        SDL::GFX::bezierRGBA(  $renderer, |random-ngon( ( 3 .. 10 ).pick ), 2, |random-rgba );
    },
    {
        SDL::GFX::gfxPrimitivesSetFontRotation( (^4).pick );
        SDL::GFX::stringColor( $renderer, |random-point, "Hello World!", random-color );
        SDL::GFX::stringRGBA(  $renderer, |random-point, "Hello World!", |random-rgba );
    },
    {
        SDL::GFX::gfxPrimitivesSetFontRotation( (^4).pick );
        SDL::GFX::characterColor( $renderer, |random-point, ( 'a' ... 'z' ).pick.ord, random-color );
        SDL::GFX::characterRGBA(  $renderer, |random-point, ( 'a' ... 'z' ).pick.ord, |random-rgba );
    },
) -> &block {
    for ^100 {
        FIRST clear-screen;
        LAST {
            present;
            sleep 1 if %*ENV<INTERACTIVE_TESTING>;
        }
        block();
        ok 1;
    }
}

done-testing;

sub random-ngon ($n) {
    use NativeCall;
    my $vx = CArray[int16].allocate($n);
    my $vy = CArray[int16].allocate($n);
    ( $vx[$_], $vy[$_] ) = random-point for ^$n;
    $vx, $vy, $n
}
sub clear-screen {
    SDL::SetRenderDrawColor( $renderer, 0, 0, 0, 255 );
    SDL::RenderClear($renderer);
}
sub present { SDL::RenderPresent($renderer) }
sub random-angle { ( 0 .. 360 ).pick }
sub random-color { random-rgba».fmt("%02x").join.parse-base(16) }
sub random-point { random-x, random-y }
sub random-radius { (^30).pick }
sub random-rgb { ( ^256).pick }
sub random-rgba { random-rgb() xx 4 }
sub random-x { ( 0 .. $width ).pick }
sub random-y { ( 0 .. $height ).pick }
