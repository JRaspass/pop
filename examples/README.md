## Pop examples

This directory has some basic examples on how to use Pop.

They are intented to be run from the root directory of the library (so one
level above this one, where the `META6.json` file is). You should be able
to run them as `./examples/foo/game` or `raku examples/foo/game`.

The spritesheet that is bundled with these examples is the [Dungeon Tileset II]
by 0x72.

[Dungeon Tileset II]: https://0x72.itch.io/dungeontileset-ii
