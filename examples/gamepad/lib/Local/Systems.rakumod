unit package Systems;

use Pop::Entities;
use Pop::Graphics;
use Pop::Inputs;
use Local::Components;

our sub render ( $axis is rw, $button is rw ) {
    use Pop::Point :operators;

    if $axis || $button {
        # Joysticks
        Pop::Entities.view(
            Position, Button, Coloured, Joystick
        ).each: -> $p, $b, $c, $j {
            Pop::Graphics.circle: :fill, $p.xy, 50, $c.lighter;
            Pop::Graphics.circle: :fill,
                $j.xy * 20 + $p.xy, 25, $b.pressed ?? $c.darker !! $c.color;
        }

        if $button {
            # Action buttons
            Pop::Entities.view(
                Position, Button, Coloured, ActionButton
            ).each: -> $p, $b, $c, $ {
                Pop::Graphics.circle: :fill, $p.xy, 25, $b.pressed ?? $c.darker !! $c.color;
                Pop::Graphics.circle: :aa,   $p.xy, 25, $c.color;
            }

            # D-Pad buttons
            Pop::Entities.view(
                Position, Button, Coloured, DPad
            ).each: -> $p, $b, $c, $ {
                Pop::Graphics.rectangle: :fill,
                    $p.xy - 20, $p.xy + 20, $b.pressed ?? $c.darker !! $c.color;
            }
        }

        if $axis {
            # Triggers
            Pop::Entities.view(
                Position, Coloured, Trigger
            ).each: -> $p, $c, $t {
                Pop::Graphics.rectangle: :fill,
                    $p.xy, $p.xy.add( 30, 60 ), $c.lighter;
                Pop::Graphics.rectangle: :fill,
                    $p.xy, $p.xy.add( 30, 60 * $t.value ), $c.darker;
            }
        }

        $axis = $button = False;
    }
}
