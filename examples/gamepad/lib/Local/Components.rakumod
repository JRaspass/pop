use Pop::Point;

class Position {
    has Pop::Point $.xy handles < x y > .= zero;
    method new ( $x, $y ) { self.bless: xy => Pop::Point.new( $x, $y ) }
}

class Movement {
    has Pop::Point $.xy handles < x y > .= zero;
    method new ( $x, $y ) { self.bless: xy => Pop::Point.new( $x, $y ) }
}

class Coloured {
    use Pop::Color;

    has Pop::Color $.color   is required;
    has Pop::Color $.lighter is required;
    has Pop::Color $.darker  is required;

    submethod BUILD ( :$color ) {
        $!color   = Pop::Color.new($color);
        $!darker  = $!color.darken:  20;
        $!lighter = $!color.lighten: 20;
    }
}

class Button {
    has       $.key     is required;
    has Bool  $.pressed is rw;
    has       $.label = $!key;
}

class DPad         { }
class ActionButton { }

class Joystick {
    has $.key is required;
    has Pop::Point $.xy is rw handles < x y > .= zero;
}

class Trigger {
    has $.key is required;
    has $.value is rw = 0;
}
