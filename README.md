NAME
====

Pop

SYNOPSIS
========

    use Pop;
    use Pop::Inputs;
    use Pop::Graphics;

    # Create your entities, maybe with Pop::Entities

    Pop.new: title => 'Revenge of the Avengers';

    Pop.update: -> $delta-in-seconds {
        # Run update systems

        # Inputs are automatically read at the beginning
        # of the main loop and exposed through Pop::Inputs
        Pop.stop if Pop::Inputs.keyboard: 'ESCAPE';
    }

    Pop.render: {
        # Run rendering systems.

        # Screen buffers will be automatically swapped after this block

        given Pop::Graphics {
            .point: ( (^.width).pick, (^.height).pick ), (^256).pick xx 3;
        }
    }

    # Run the main loop
    Pop.run;

DESCRIPTION
===========

Pop is an experimental 2D game development framework for Raku. It has been inspired by frameworks like LÖVE2D and Pico-8, and by others of a similar nature. It runs on SDL2 using custom bindings.

This project is still in its infancy, and the interface might change without warning. Any changes (breaking or otherwise) will still be reported in the change log, so that is the first place to check before updating.

External Dependencies
---------------------

Since most of the heavy lifting is done using SDL2, Pop requires some external libraries in order to work. In particular, it requires the following libraries to be available in your system:

  * SDL2

  * SDL2_image

  * SDL2_gfx

Please refer to your system's documentation for details on how to obtain these.

CONCEPTS
========

Singletons
----------

Most core classes (including the main Pop game class as well as helpers like Pop::Graphics and Pop::inputs) are implemented as globally available singletons. This means that, unless specific parameters need to be set during initialisation, there is no need to manually construct them and pass references to them, which should help simplify game code.

The caveat to this is that, since they will be constructed automatically if needed, any manual initialisation needs to be done as early as possible.

Configuration
-------------

Most default values used throughout Pop can be specified via the global Pop::Config singleton (see the documentation of that module for more details).

The values in Pop::Config will be read from a file named `config.toml` in the same directory as the mainline entrypoint. To read the global configuration from a different path, pass that path to `Pop::Config.new` as early as possible.

METHODS
=======

new
---

    method new (|c) returns Pop

Initialises a new global game object.

This function will be called automatically when any method is called on `Pop`, so it should be called as early as possible unless the default values are appropriate.

The parameters passed in will be forwarded to the initialisation of the global [Pop::Graphics](Pop::Graphics) singleton. See that module for more details about acceptable parameters and their meanings.

Default values are those specified by [Pop::Config](Pop::Config). See that module for more details.

run
---

    method new (
        :$fps = 60,
        :$dt,
    ) returns Nil

Starts the execution a game's main loop. The main loop will continue to execute until `stop` is called, either directly or through `handle-input`.

The default main loop has roughly the following stages:

  * `handle-input` is called, to process input events.

  * The `update` block is executed as many times as needed.

    If the `$dt` parameter was set, then the block will be called as long as that amount of time has accumulated since the last time it was called. Otherwise, the block will be called once with however much time has elapsed since the last frame.

  * The `render` block gets called, as long as the time spent on the frame is not already longer than the time needed to maintain the framerate specified in the `$fps` parameter (which defaults to 60).

    If the `$fps` parameter was explicitly disabled (eg. with `:!fps`) then the `render` block will be called once per frame.

stop
----

    method stop () returns Nil

Stop execution of a game, which started the last time `run` was called.

handle-input
------------

    method handle-input () returns Nil

Process input events and update the state of Pop::Inputs. See the documentation for [Pop::Inputs.update](Pop::Inputs.update) for more details.

Calling `handle-input` will automatically call `stop` when a QUIT event is received (ie. when the game window is closed).

delta
-----

    method delta () returns Duration

Returns a Duration object representing the time elapsed since the last frame started.

wait
----

    method wait (
        Int :$dt,
    ) returns Nil

Stop execution for at least the specified number of seconds.

destroy
-------

    method destroy () returns Nil

Frees any resources that have been loaded and cached by the game.

MAIN CALLBACKS
==============

update
------

    method update (
        &update,
    ) returns Nil

Set a code block to be run to update the game state on every game frame. The code will be called with the delta time in seconds as the only parameter.

The `update` block runs before the `render` block (see below), possibly multiple times per frame, depending on the values passed to `run`. See the entry for that method for details on the structure of the default main loop.

render
------

    method render (
        &render,
    ) returns Nil

Set a code block to be run to render the game state. This block will be called up to once per frame, with no arguments.

For details on the aspects of the main loop that control whether the `render` block gets called or not, see the entry for the `run` method above.

INPUT CALLBACKS
===============

The callbacks listed below are shortcuts for the callbacks of the same name in Pop::Inputs. For more details, see that module's documentation.

key-pressed
-----------

    method key-pressed ( &cb ) returns Nil

key-released
------------

    method key-released ( &cb ) returns Nil

mouse-moved
-----------

    method mouse-moved ( &cb ) returns Nil

wheel-moved
-----------

    method wheel-moved ( &cb ) returns Nil

mouse-pressed
-------------

    method mouse-pressed ( &cb ) returns Nil

mouse-released
--------------

    method mouse-released ( &cb ) returns Nil

controller-moved
----------------

    method controller-moved ( &cb ) returns Nil

controller-pressed
------------------

    method controller-pressed ( &cb ) returns Nil

controller-released
-------------------

    method controller-released ( &cb ) returns Nil

file-dropped
------------

    method file-dropped ( &cb ) returns Nil

text-dropped
------------

    method text-dropped ( &cb ) returns Nil

text-input
----------

    method text-input ( &cb ) returns Nil

text-edited
-----------

    method text-edited ( &cb ) returns Nil

WINDOW CALLBACKS
================

The callbacks listed below are shortcuts for the callbacks of the same name in Pop::Inputs. For more details, see that module's documentation.

focused
-------

    method focused ( &cb ) returns Nil

mouse-focused
-------------

    method mouse-focused ( &cb ) returns Nil

window-exposed
--------------

    method window-exposed ( &cb ) returns Nil

window-moved
------------

    method window-moved ( &cb ) returns Nil

window-resized
--------------

    method window-resized ( &cb ) returns Nil

window-visible
--------------

    method window-visible ( &cb ) returns Nil

ACKNOWLEDGEMENTS
================

The core ECS logic in Pop::Entities has been heavily influenced, both in its interface and its implementation, by [EnTT](https://github.com/skypjack/entt).

Some of the slicing of the singleton classes is based on that seen in popular game development frameworks, but were most directly influenced by [LÖVE2D](https://love2d.org).

The singleton role used in this distribution is also a greatly simplified version of [Staticish](https://github.com/jonathanstowe/Staticish), while the code in Pop::Point derives from a modified version of [Point](https://github.com/0racle/raku-Point).

COPYRIGHT AND LICENSE
=====================

Copyright 2021 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.
