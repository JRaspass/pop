#!/usr/bin/env raku

use Test;
use Pop::Point;

subtest 'Constructors' => {
    subtest 'Zero' => {
        my $p = Pop::Point.zero;

        is $p.length,        0, 'length';
        is $p.normal.length, 0, 'normal';

        is ~$p, '(0, 0)',  'Str';
        is +$p, $p.length, 'Numeric';
        is ?$p, False,     'Bool';

        is ( $p.x, $p.y ), ( 0, 0 ), 'x and y accessors';
    }

    subtest 'Integers' => {
        my $p = Pop::Point.new: 3, 4;

        is $p.length,        5, 'length';
        is $p.normal.length, 1, 'normal';

        is ~$p, '(3, 4)',  'Str';
        is +$p, $p.length, 'Numeric';
        is ?$p, True,      'Bool';
    }

    subtest 'Rational' => {
        my $p = Pop::Point.new: 1.5;

        is $p.length.Rat,    2.12132, 'length';
        is $p.normal.length, 1,       'normal';

        is ~$p, '(1.5, 1.5)', 'Str';
        is +$p, $p.length,    'Numeric';
        is ?$p, True,         'Bool';
    }

    subtest 'Negative' => {
        my $p = Pop::Point.new: -3, -4;

        is $p.length,        5, 'length';
        is $p.normal.length, 1, 'normal';

        is ~$p, '(-3, -4)', 'Str';
        is +$p, $p.length,  'Numeric';
        is ?$p, True,       'Bool';
    }

    subtest 'Undef' => {
        my $p = Pop::Point;

        dies-ok { $p.length }, 'length';
        dies-ok { $p.normal }, 'normal';

        warns-like { is ~$p, '', 'Str'     }, / 'string context' /;
        warns-like { is +$p, 0,  'Numeric' }, / 'numeric context' /;

        is ?$p, False, 'Bool';
    }
}

subtest 'Operations' => {
    use Pop::Point :operators;

    my $z = Pop::Point.zero;
    my $a = Pop::Point.new: 1, 3;
    my $b = Pop::Point.new: 2, 1;

    is "{ $a + $b }", '(3, 4)',   'point + point';
    is "{ $a - $b }", '(-1, 2)',  'point - point';
    is "{ $a * $b }", '(2, 3)',   'point * point';
    is "{ $a / $b }", '(0.5, 3)', 'point / point';

    is "{ $a + 2 }", '(3, 5)',     'point + number';
    is "{ $a - 2 }", '(-1, 1)',    'point - number';
    is "{ $a * 2 }", '(2, 6)',     'point * number';
    is "{ $a / 2 }", '(0.5, 1.5)', 'point / number';

    is 2 + $b, 2 + $b.length, 'number + point';
    is 2 - $b, 2 - $b.length, 'number - point';
    is 2 * $b, 2 * $b.length, 'number * point';
    is 2 / $b, 2 / $b.length, 'number / point';

    is $a <=> $b, More, 'More <=>';
    is $b <=> $a, Less, 'Less <=>';
    is $b + 1 <=> $b + 1, Same, 'Same <=>';

    is $a cmp $b, More, 'More cmp';
    is $b cmp $a, Less, 'Less cmp';
    is $b + 1 cmp $b + 1, Same, 'Same cmp';

    is $a ~~ $a.length, True, '~~ uses length on Numeric';
}

subtest 'Methods' => {
    subtest 'On self' => {
        my $p = Pop::Point.new: 1.50, 2.49;

        is $p.round.list,   ( 2, 2 ), 'round';
        is $p.floor.list,   ( 1, 2 ), 'floor';
        is $p.ceiling.list, ( 2, 3 ), 'ceiling';

        is Pop::Point.new(  1,  0 ).angle.list,   0,      'angle right';
        is Pop::Point.new( -1,  0 ).angle.list,   π,      'angle left';
        is Pop::Point.new(  0,  1 ).angle.list,   π / 2 , 'angle down';
        is Pop::Point.new(  0, -1 ).angle.list, - π / 2 , 'angle up';
    }

    subtest 'On other' => {
        my $p = Pop::Point.new: 5, 5;

        is $p.angle( $p.add:  1,  0 ).list,   0,      'angle right';
        is $p.angle( $p.add: -1,  0 ).list,   π,      'angle left';
        is $p.angle( $p.add:  0,  1 ).list,   π / 2 , 'angle down';
        is $p.angle( $p.add:  0, -1 ).list, - π / 2 , 'angle up';
    }
}

subtest 'Coercions' => {
    subtest 'Single number' => {
        my Pop::Point() $p = 5;
        is $p.list, ( 5, 5 ), 'Sets single number as both components';
    }

    subtest 'List with two numbers' => {
        my Pop::Point() $p = ( 4, 3 );
        is $p.list, ( 4, 3 ), 'Sets each number to a differet component';
    }
}

done-testing;

# This feels like it should exist?
sub warns-like ( &code, $message, $name? ) {
    CONTROL {
        when CX::Warn { like .message, $message, $name // Empty; .resume }
    }
    &code.();
}
