#!/usr/bin/env raku

use Test;
use Pop::Rect;

subtest 'Constructors' => {
    subtest 'Zero' => {
        my $r = Pop::Rect.zero;

        is ~$r, '(0, 0, 0, 0)', 'Str';
        is ?$r, False,          'Bool';

        is ( $r.x, $r.y ), ( 0, 0 ), 'x and y accessors';
        is ( $r.w, $r.h ), ( 0, 0 ), 'w and h accessors';
    }

    subtest 'Single number' => {
        my $r = Pop::Rect.new: 3;

        is ~$r, '(0, 0, 3, 3)', 'Str';
        is ?$r, True,           'Bool';
    }

    subtest 'Two numbers' => {
        my $r = Pop::Rect.new: 3, 4;

        is ~$r, '(0, 0, 3, 4)', 'Str';
        is ?$r, True,           'Bool';
    }

    subtest 'Four numbers' => {
        my $r = Pop::Rect.new: 1, 2, 3, 4;

        is ~$r, '(1, 2, 3, 4)', 'Str';
        is ?$r, True,           'Bool';
    }

    subtest 'Undef' => {
        my $r = Pop::Rect;

        warns-like { is ~$r, '', 'Str' }, / 'string context' /;
        is ?$r, False, 'Bool';
    }
}

subtest 'Methods' => {
    my $r = Pop::Rect.new: 1, 2, 3, 4;

    is $r.area, 12, 'area';

    is ~$r.translate( 3 ),    '(4, 5, 3, 4)', 'translate with number';
    is ~$r.translate( 3, 2 ), '(4, 4, 3, 4)', 'translate with tuple';

    is ~$r.translate( Pop::Point.new( 1, 1 ) ), '(2, 3, 3, 4)',
        'translate with point';

    is Pop::Rect.new( 1.49, 1.5 ).round.list, ( 0, 0, 1, 2 ), 'round';
}

subtest 'Collide' => {
    subtest 'With point' => {
        my \P := Pop::Point;
        my $a = Pop::Rect.new: 1, 1;

        is $a.collide( P.new( 0.5, 0.5 ) ), True,  'on centre';
        is $a.collide( P.new( 0.5, 0   ) ), True,  'on top edge';
        is $a.collide( P.new( 0,   0.5 ) ), True,  'on left edge';
        is $a.collide( P.new( 0,   0   ) ), True,  'on top-left corner';

        is $a.collide( P.new( 1,   0.5 ) ), False, 'on right edge';
        is $a.collide( P.new( 0.5, 1   ) ), False, 'on bottom edge';
        is $a.collide( P.new( 1,   0   ) ), False, 'on top-right corner';
        is $a.collide( P.new( 0,   1   ) ), False, 'on bottom-left corner';
        is $a.collide( P.new( 1,   1   ) ), False, 'on bottom-right corner';
    }

    subtest 'With rectangle' => {
        my \R := Pop::Rect;
        my $a = R.new: 10, 10;

        is $a.collide( R.new(  11,   0, 10, 10 ) ), False, 'flush right';
        is $a.collide( R.new( -11,   0, 10, 10 ) ), False, 'flush left';
        is $a.collide( R.new(   0, -11, 10, 10 ) ), False, 'flush top';
        is $a.collide( R.new(   0,  11, 10, 10 ) ), False, 'flush bottom';

        is $a.collide( R.new(  10,   0, 10, 10 ) ), True,  'edge right';
        is $a.collide( R.new( -10,   0, 10, 10 ) ), True,  'edge left';
        is $a.collide( R.new(   0, -10, 10, 10 ) ), True,  'edge top';
        is $a.collide( R.new(   0,  10, 10, 10 ) ), True,  'edge bottom';
    }
}

subtest 'Coercions' => {
    subtest 'Single number' => {
        my Pop::Rect() $r = 5;
        is ( |$r ), ( 0, 0, 5, 5 ), 'Sets single number as width and height';
    }

    subtest 'List with two numbers' => {
        my Pop::Rect() $r = ( 4, 3 );
        is ( |$r ), ( 0, 0, 4, 3 ), 'Sets numbers as width and height';
    }

    subtest 'List with two points' => {
        my Pop::Rect() $r = ( Pop::Point.new( 1, 2 ), Pop::Point.new( 3, 4 ) );
        is ( |$r ), ( 1, 2, 3, 4 ), 'Sets first as xy, second as wh';
    }

    subtest 'List with two two-numbered lists' => {
        my Pop::Rect() $r = ( ( 1, 2 ), ( 3, 4 ) );
        is ( |$r ), ( 1, 2, 3, 4 ), 'Sets first as xy, second as wh';
    }

    subtest 'List with four numbers' => {
        my Pop::Rect() $r = ( 4, 3, 2, 1 );
        is ( |$r ), ( 4, 3, 2, 1 ), 'Sets numbers as x, y, w, and h';
    }
}

done-testing;

# This feels like it should exist?
sub warns-like ( &code, $message, $name = 'Warns OK' ) {
    CONTROL {
        when CX::Warn { like .message, $message, $name // Empty; .resume }
    }
    &code.();
}
