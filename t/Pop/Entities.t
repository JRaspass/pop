#!/usr/bin/env raku

use Test;
use Pop::Entities;

class Named { has Str $.name }
role  Aging { has Int $.age }
class Other { }

my \E := Pop::Entities;

subtest 'Basic operations' => {
    E.clear;

    my $a = E.create;
    my $b = E.create;

    nok E.check( $a, Named ), 'A does not have Named';

    E.add: $a, Named.new( name => 'old' );
    E.add: $a, Named.new( name => 'new' );

     ok E.check( $a, Named ), 'A has Named';
    nok E.check( $b, Named ), 'B does not have Named';

    is E.get( $a, Named ).name, 'new', 'Adding component replaces';

    is E.delete( $a, Named ), Nil, 'delete returns Nil';

    nok E.check( $a, Named ), 'A does not have Named';

    E.delete: $a, Named;

    nok E.check( $a, Named ), 'Deleting is idempotent';

    is E.get( $a, Named ), Named, 'Getting returns type object';
}

subtest 'Delete entities' => {
    E.clear;

    my $a = E.create: Named.new( name => 'a' ), Other;
    E.delete: $a;

    nok E.get( $a, Other ), 'A does not have Other';
    nok E.get( $a, Named ), 'A does not have Named';
}

subtest 'Recycling GUIDs' => {
    E.clear;

    is E.alive, 0, 'Right number of alive entities when none created';

    # Create 10 entities; will use the first 10 entity IDs ( 0 .. 9 )
    my @e = (^10).map: { E.create }

    is E.alive, 10, 'Right number of alive entities when all alive';
    is E.valid(9),  True,  'Entity is valid';
    is E.valid(10), False, 'Entity is not valid';

    # Delete the entities we've just generated
    # Will mark their IDs as ready to be recycled
    E.delete: $_ for @e;

    is E.alive, 0, 'Right number of alive entities when all dead';
    is E.valid(9),  False, 'Entity is not valid';

    # Create 20 entities
    # They should re-use the first 10 IDs and use the next 10 ( 0 .. 19 )
    is (^20).map({ E.create(Other) +& 0xFFFFF }).sort, ^20,
        'Recycled and generated the right IDs';

    is E.valid(9),  False, 'Entity is valid';
    is E.alive, 20, 'Recorded the right number of alive entities after recycling';
    is E.view(Other).map({1}).sum, 20, 'Only alive entities match view';
}

subtest 'Simple view' => {
    E.clear;

    my $named   = E.create;
    my $aging   = E.create;
    my $both    = E.create;
    my $dead    = E.create;
    my $reverse = E.create;
    my $extra   = E.create;

    E.delete: $dead;

    E.add: $named, Named.new: name => 'Pat';
    E.add: $aging, Aging.new: age  => 10;

    E.add: $both, Aging.new: age  => 20;
    E.add: $both, Named.new: name => 'Tim';

    E.add: $reverse, Named.new: name => 'Mit';
    E.add: $reverse, Aging.new: age  => 2;

    E.add: $extra, Named.new: name => 'Most';
    E.add: $extra, Aging.new: age  => 200;
    E.add: $extra, Other.new;

    my SetHash $set .= new;
    for E.view( Named ) {
        with E.get( $_, Named ).name {
            # diag "My name is $_";
            $set.set: $_;
        }
    }

    ok $set ~~ set(< Pat Tim Mit Most >),
        'Iterate over simple view with for';

    $set .= new;
    E.view( Aging ).each: -> $a {
        # diag "I am { $a.age } years old";
        $set.set: $a.age;
    }

    ok $set ~~ set( 2, 10, 20, 200 ), 'Iterate over simple view with each';

    $set .= new;
    for E.view( Aging, Named ) {
        my $a = E.get: $_, Aging;
        my $n = E.get: $_, Named;
        # diag "My name is { $n.name }, and I am { $a.age } years old";
        $set.set: $n.name ~ ':' ~ $a.age;
    }

    ok $set ~~ set(< Tim:20 Mit:2 Most:200 >),
        'Iterate over complex view with for';

    $set .= new;
    E.view( Named, Aging ).each: -> $n, $a {
        # diag "My name is { $n.name }, and I am { $a.age } years old";
        $set.set: $n.name ~ ':' ~ $a.age;
    }

    ok $set ~~ set(< Tim:20 Mit:2 Most:200 >),
        'Iterate over complex view with each, arity matches view';

    $set .= new;
    E.view( Aging, Named ).each: -> $e, $, $n {
        my $a = E.get: $e, Aging;
        # diag "My name is { $n.name }, and I am { $a.age } years old";
        $set.set: $n.name ~ ':' ~ $a.age;
    }

    ok $set ~~ set(< Tim:20 Mit:2 Most:200 >),
        'Iterate over complex view with each, arity includes entity';

    $set .= new;
    E.view( Aging, Named ).each: -> $e, *@c {
        # diag "My name is { @c[1].name }, and I am { @c[0].age } years old";
        $set.set: @c[1].name ~ ':' ~ @c[0].age;
    }

    ok $set ~~ set(< Tim:20 Mit:2 Most:200 >),
        'Iterate over complex view with variadic block';

    $set .= new;
    E.view(*).each: -> $e {
        $set.set: $e ~ ':' ~ E.get( $e, Aging ).defined;
    }

    ok $set ~~ set(< 0:False 1:True 2:True 4:True 5:True >),
        'Iterate over all entities';
}

subtest 'Modifying view' => {
    E.clear;

    my class X      { has $.value is rw = 10 }
    my class Y is X { }

    E.create: Y.new( value => 7 ), X.new( value => 7 );
    E.create: Y.new( value => 5 ), X.new( value => 5 );
    E.create: Y.new,               X.new;

    E.view(X, Y).each: -> \e, \x, \y {
        if --x.value < 5 {
            E.delete: e;
            E.create: X.new( value => 11 );
            E.create: Y.new( value => 11 );
        }
    }

    my @x = gather E.view(*).each: -> \e { take .value with E.get: e, X }
    ok @x.sort ~~ ( 6, 9, 11 );

    my @y = gather E.view(*).each: -> \e { take .value with E.get: e, Y }
    ok @y.sort ~~ ( 7, 10, 11 );
}

done-testing;
